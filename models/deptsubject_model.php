<?php
/**
 * main page model example
 *
 * */
class Deptsubject_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function getDept() {

		$stmt = $this->db->prepare("SELECT * FROM department");
		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function getSubjects() {

		$stmt = $this->db->prepare("SELECT * FROM subjects");
		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function addSubject($code, $department) {

		$stmt = $this->db->prepare("INSERT INTO subjects (code, department) VALUES(:code, :department)");
		$stmt->execute(array(":code" => $code, ":department" => $department));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function addDepartment($dept_name) {

		$stmt = $this->db->prepare("INSERT INTO department (dept_name) VALUES(:dept_name)");
		$stmt->execute(array(":dept_name" => $dept_name));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}
}
