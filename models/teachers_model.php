<?php
/**
 * main page model example
 *
 * */
class Teachers_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function getAll() {

		$stmt = $this->db->prepare("SELECT * FROM teachers ORDER BY lastname ASC");
		$stmt->execute();

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function getDepartment() {

		$stmt = $this->db->prepare("SELECT * FROM department");
		$stmt->execute();

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function getSubjects() {

		$stmt = $this->db->prepare("SELECT * FROM subjects");
		$stmt->execute();

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function getStudentTeacher($id) {

		$stmt = $this->db->prepare("SELECT DISTINCT * FROM teachers WHERE department=:department");
		$stmt->execute(array(":department" => $id));

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function getStudentSubject($id) {

		$stmt = $this->db->prepare("SELECT DISTINCT s.id, s.* FROM subjects as s LEFT JOIN users as u ON s.id = u.subject WHERE u.student_id = :id");
		$stmt->execute(array(":id" => $id));

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function add($firstname, $middlename, $lastname, $department) {

		$stmt = $this->db->prepare("INSERT INTO teachers (firstname, middlename, lastname, department) VALUES(:firstname, :middlename, :lastname, :department)");
		$stmt->execute(array(":firstname" => $firstname, ":middlename" => $middlename, ":lastname" => $lastname, ":department" => $department));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function edit($firstname, $middlename, $lastname, $department, $id) {

		$stmt = $this->db->prepare("UPDATE teachers SET firstname=:firstname, middlename=:middlename, lastname=:lastname, department=:department WHERE id=:id LIMIT 1");
		$stmt->execute(array(":firstname" => $firstname, ":middlename" => $middlename, ":lastname" => $lastname, ":department" => $department, ":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete($id) {

		$stmt = $this->db->prepare("DELETE FROM teachers WHERE id=:id");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function getTeacher($id) {

		$stmt = $this->db->prepare("SELECT * FROM teachers WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}

	public function getTeachers($department, $teacher, $subject, $semester) {

		if ($teacher == "-" && $subject != "-") { #if teacher is null and subject is not
			$stmt = $this->db->prepare("SELECT t.*, u.* FROM teachers as t LEFT JOIN users as u ON t.id = u.teacher WHERE u.subject=:subject AND t.department=:department");
			$stmt->execute(array(":subject" => $subject, ":department" => $department));			
		} else if ($teacher != "-" && $subject == "-") { #if teacher is not null and subject is null
			$stmt = $this->db->prepare("SELECT * FROM teachers WHERE id=:teacher AND department=:department");
			$stmt->execute(array(":teacher" => $teacher, ":department" => $department));
		} else if ($teacher != "-" && $subject != "-") { #if both teacher and subject is not null
			$stmt = $this->db->prepare("SELECT t.*, u.* FROM teachers as t LEFT JOIN users as u ON t.id = u.teacher WHERE u.teacher=:teacher AND u.subject=:subject AND t.department=:department");
			$stmt->execute(array(":teacher" => $teacher, ":subject" => $subject, ":department" => $department));			
		} else {
			$stmt = $this->db->prepare("SELECT * FROM teachers WHERE department=:department");
			$stmt->execute(array(":department" => $department));			
		}

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}

	public function search_teachers($sql) {

		$stmt = $this->db->query($sql);
		//$stmt->execute();

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}		

	}

	public function count_eval_teacher($teacher, $department) {

		$stmt = $this->db->prepare("SELECT COUNT(id) as count FROM evaluation WHERE teacher_id=:teacher_id AND department=:department");
		$stmt->execute(array(":teacher_id" => $teacher, ":department" => $department));

		$rsCountEval = $stmt->fetch(PDO::FETCH_ASSOC);

		return $rsCountEval['count'];
	}

	public function getTeachersv3($sql) {

		$stmt = $this->db->query($sql);
		//$stmt->execute();

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}	

	}

	public function getTeachersComments($t_id, $t_department, $t_subject) {

		$stmt = $this->db->prepare("SELECT * FROM evaluation WHERE teacher_id=:t_id AND department=:t_department AND subject_id=:t_subject");
		$stmt->execute(array(":t_id" => $t_id, ":t_department" => $t_department, ":t_subject" => $t_subject));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}

	public function preSQLQuery($sql) {

		$stmt = $this->db->query($sql);
		//$stmt->execute();

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}		

	}

}
