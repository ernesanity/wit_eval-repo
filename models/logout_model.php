<?php
/**
 * main page model example
 *
 * */
class Logout_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	function logout(Session $session) {
		$session->set('user', null);

		$session->destroy();
		header('refresh:3;url=' . BASEPATH);
	}

} //class Logout_Model extends Model