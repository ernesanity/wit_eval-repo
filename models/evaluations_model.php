<?php
/**
 * main page model example
 *
 * */
class Evaluations_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function getTeachers($department, $type) {

		if ($type == "student") {
			$stmt = $this->db->prepare("SELECT * FROM teachers WHERE department=:department");
			$stmt->execute(array(":department" => $department));
		} else {
			$stmt = $this->db->prepare("SELECT * FROM teachers");
			$stmt->execute();			
		}

		$rs = $stmt->fetchAll();
		return $rs;
	}


	public function checkTeacher($id) {

		$stmt = $this->db->prepare("SELECT teacher_id FROM evaluation WHERE teacher_id=:teacher_id");
		$stmt->execute(array(":teacher_id" => $id));

		if ($stmt->rowCount() > 0) {
			return true; #if true, teacher eval is already existing
		} else {
			return false; #if false, teacher eval is not yet existing
		}

	}

	public function postEvaluate($score, $strong_points, $improve_teaching, $other, $teacher_id, $subject_id, $department, $student_id) {

		$stmt = $this->db->prepare("INSERT INTO evaluation (score, strong_points, improve_teaching, other, teacher_id, subject_id, department, student_id) VALUES(:score, :strong_points, :improve_teaching, :other, :teacher_id, :subject_id, :department, :student_id)");
		$stmt->execute(array(":score" => $score, ":strong_points" => $strong_points, ":improve_teaching" => $improve_teaching, ":other" => $other, ":teacher_id" => $teacher_id, ":subject_id" => $subject_id, ":department" => $department, ":student_id" => $student_id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function search_teachers($sql) {

		$stmt = $this->db->query($sql);
		//$stmt->execute();

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}		

	}

	public function getSubjectCode($id) {

		$stmt = $this->db->prepare("SELECT * FROM subjects WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetch(PDO::FETCH_ASSOC);
			return $rs['code'];
		} else {
			return false;
		}		

	}

	public function getTeacher($id) {

		$stmt = $this->db->prepare("SELECT * FROM teachers WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}


	public function getSubject($id) {

		$stmt = $this->db->prepare("SELECT * FROM subjects WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}


}
