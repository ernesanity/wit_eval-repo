<?php
/**
 * main page model example
 *
 * */
class Users_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function getAll() {

		$stmt = $this->db->prepare("SELECT * FROM students ORDER BY lastname ASC");
		$stmt->execute();

		$rs = $stmt->fetchAll();

		return $rs;

	}

	public function add($student_num, $firstname, $middlename, $lastname, $dob, $street_brgy, $city, $province, $course, $age, $year, $email, $department, $password) {

		$stmt = $this->db->prepare("INSERT INTO students (stud_num, firstname, middlename, lastname, dob, st_brgy, city, province, course, age, year, email, department, password, type) VALUES(:stud_num, :firstname, :middlename, :lastname, :dob, :street_brgy, :city, :province, :course, :age, :year, :email, :department, :password, :type)");
		$stmt->execute(array(":stud_num" => $student_num, ":firstname" => $firstname, ":middlename" => $middlename, ":lastname" => $lastname, ":dob" => $dob, ":street_brgy" => $street_brgy, ":city" => $city, ":province" => $province, ":course" => $course, ":age" => $age, ":year" => $year, ":email" => $email, ":department" => $department, ":password" => md5($password), ":type" => "student"));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function edit($student_num, $firstname, $middlename, $lastname, $dob, $street_brgy, $city, $province, $course, $age, $year, $email, $department, $id) {

		$stmt = $this->db->prepare("UPDATE students SET stud_num=:stud_num, firstname=:firstname, middlename=:middlename, lastname=:lastname, dob=:dob, st_brgy=:st_brgy, city=:city, province=:province, course=:course, age=:age, year=:year, email=:email, department=:department WHERE id=:id LIMIT 1");
		$stmt->execute(array(":stud_num" => $student_num, ":firstname" => $firstname, ":middlename" => $middlename, ":lastname" => $lastname, ":dob" => $dob, ":st_brgy" => $street_brgy, ":city" => $city, ":province" => $province, ":course" => $course, ":age" => $age, ":year" => $year, ":email" => $email, ":department" => $department, ":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete($id) {

		$stmt = $this->db->prepare("DELETE FROM students WHERE id=:id");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function getDept() {

		$stmt = $this->db->prepare("SELECT * FROM department");
		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function getTeachers() {

		$stmt = $this->db->prepare("SELECT * FROM teachers");
		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function getSubjects() {

		$stmt = $this->db->prepare("SELECT * FROM subjects");
		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function checkUser($teacher, $subject, $id) {

		$stmt = $this->db->prepare("SELECT * FROM users WHERE teacher = :teacher AND subject = :subject AND student_id = :student_id");
		$stmt->execute(array(":teacher" => $teacher, ":subject" => $subject, ":student_id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function addUser($school_year, $semester, $department, $teacher, $subject, $title, $student_id) {

		$stmt = $this->db->prepare("INSERT INTO users (school_year, semester, department, teacher, subject, title, student_id) VALUES (:school_year, :semester, :department, :teacher, :subject, :title, :student_id)");
		$stmt->execute(array(":school_year" => $school_year, ":semester" => $semester, ":department" => $department, ":teacher" => $teacher, ":subject" => $subject, ":title" => $title, ":student_id" => $student_id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}


	}

	public function getStudent($id) {

		$stmt = $this->db->prepare("SELECT * FROM students WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}

	public function search($department, $keyword) {

		if ($department != "-" && $keyword == "" || $department != "-" && $keyword == null) {
			$stmt = $this->db->prepare("SELECT * FROM students WHERE department=:department ORDER BY lastname ASC");
			$stmt->execute(array(":department" => $department));
		} else if ($department == "-" && $keyword != "") {
			$stmt = $this->db->prepare("SELECT * FROM students WHERE firstname LIKE :keyword OR middlename LIKE :keyword OR lastname LIKE :keyword ORDER BY lastname ASC");
			$keyword = "%".$keyword."%";
			$stmt->execute(array(":keyword" => $keyword));
		} else if ($department != "-" && $keyword != "") {
			$stmt = $this->db->prepare("SELECT * FROM students WHERE firstname LIKE :keyword AND department=:department OR middlename LIKE :keyword  AND department=:department OR lastname LIKE :keyword  AND department=:department ORDER BY lastname ASC");
			$keyword = "%".$keyword."%";
			$stmt->execute(array(":keyword" => $keyword, ":department" => $department));
		}

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		} 
	}

	public function getTeacherByDepartment($department) {

		$stmt = $this->db->prepare("SELECT * FROM teachers WHERE department=:department");
		$stmt->execute(array(":department" => $department));

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function getSubjectByDepartment($department) {

		$stmt = $this->db->prepare("SELECT * FROM subjects WHERE department=:department");
		$stmt->execute(array(":department" => $department));

		if ($stmt->rowCount() > 0) {
			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function getTeacher($id) {

		$stmt = $this->db->prepare("SELECT * FROM teachers WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}


	public function getSubject($id) {

		$stmt = $this->db->prepare("SELECT * FROM subjects WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}

	}

	public function preSQLQuery($sql) {

		$stmt = $this->db->query($sql);
		//$stmt->execute();

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;

		} else {

			return false;
		}		

	}

}
