<?php
/**
 * main page model example
 *
 * */
class Main_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function validateLogin($username, $password) {

		#for admin
		$stmt = $this->db->prepare("SELECT * FROM admin WHERE username=:username AND password=:password LIMIT 1");
		$stmt->execute(array(":username" => $username, ":password" => $password));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetch(PDO::FETCH_ASSOC);

			$_SESSION['user'] = array(
									"id" => $rs['id'],
									"firstname" => $rs['firstname'],
									"middlename" => $rs['middlename'],
									"lastname" => $rs['lastname'],
									"department" => $rs['department'],
									"type" => $rs['type']
									//"firsttime_login" => $rs['firsttime_login']
									);

			return $rs['type'];
		} else {

			$stmt2 = $this->db->prepare("SELECT * FROM students WHERE stud_num=:username AND password=:password LIMIT 1");
			$stmt2->execute(array(":username" => $username, ":password" => $password));

			if ($stmt2->rowCount() > 0) {

			$rs2 = $stmt2->fetch(PDO::FETCH_ASSOC);

			$_SESSION['user'] = array(
									"id" => $rs2['id'],
									"stud_num" => $rs2['stud_num'],
									"firstname" => $rs2['firstname'],
									"middlename" => $rs2['middlename'],
									"lastname" => $rs2['lastname'],
									"department" => $rs2['department'],
									"type" => $rs2['type']
									//"firsttime_login" => $rs['firsttime_login']
									);

				return $rs2['type'];
			} else {
				return false;
			}
		}

	}

}
