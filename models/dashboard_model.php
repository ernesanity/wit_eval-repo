<?php
/**
 * main page model example
 *
 * */
class Dashboard_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function getStudentData($id) {

		$stmt = $this->db->prepare("SELECT * FROM students WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {

			$rs = $stmt->fetchAll();

			return $rs;
		} else {
			return false;
		}

	}

	public function assessment($q1, $q2, $q3, $q4, $q5, $u_id) {

		$score = 0;

		if ($q1 == 2) {
			$score+=1;
		} else {
			$score+=0;
		}

		if ($q2 == 4) {
			$score+=1;
		} else {
			$score+=0;
		}

		if ($q3 == 6) {
			$score+=1;
		} else {
			$score+=0;
		}

		if ($q2 == 8) {
			$score+=1;
		} else {
			$score+=0;
		}

		if ($q2 == 10) {
			$score+=1;
		} else {
			$score+=0;
		}

		$stmt = $this->db->prepare("INSERT INTO assessment (u_id, score) VALUES(:u_id, :score)");
		$stmt->execute(array(":u_id" => $u_id, ":score" => $score));

		if ($stmt->rowCount() > 0) {
			$updateAssessment = $this->db->prepare("UPDATE users SET firsttime_login=:val WHERE id=:u_id");
			$updateAssessment->execute(array(":val" => 1, ":u_id" => $u_id));


			return true;
		} else {
			return false;
		}

	}

}
