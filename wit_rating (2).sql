-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2018 at 01:54 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wit_rating`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `firstname`, `middlename`, `lastname`, `username`, `password`, `department`, `type`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'all', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `assessment`
--

CREATE TABLE `assessment` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment`
--

INSERT INTO `assessment` (`id`, `u_id`, `score`) VALUES
(1, 19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_name`) VALUES
(1, 'BSIT'),
(2, 'BSCE'),
(3, 'BSME'),
(5, 'BSEE'),
(6, 'BSCompE');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `strong_points` text NOT NULL,
  `improve_teaching` text NOT NULL,
  `other` text NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluation`
--

INSERT INTO `evaluation` (`id`, `score`, `strong_points`, `improve_teaching`, `other`, `teacher_id`, `subject_id`, `department`, `student_id`) VALUES
(1, 67, 'TALL DARK AND HANDSOME SHIT', 'no classes every meeting', 'sakita ko gha', 7, 7, 1, 8),
(2, 80, 'z,dxjbvkjdb aslkfjhksajfh', '.skdnhfihfojuh', '.skdjhfksuuhf', 7, 7, 1, 9),
(22, 16, '', '', '', 16, 12, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20171226031741');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `stud_num` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `st_brgy` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `year` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `department` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firsttime_login` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `stud_num`, `firstname`, `middlename`, `lastname`, `dob`, `st_brgy`, `city`, `province`, `course`, `age`, `year`, `email`, `department`, `password`, `firsttime_login`, `type`) VALUES
(9, 31407, 'Shaina ', 'G', 'Carbonilla', '1998-11-23', 'Brgy: Timawa', 'Iloilo', 'Iloilo', 'BSIT', 19, '4th', 'shaneebabs33@gmail.com', 1, '4297f44b13955235245b2497399d7a93', 0, 'student'),
(10, 31408, 'Arwin', '', 'Villegas', '1994-10-21', 'Lapaz', 'Iloilo', 'Iloilo', 'BSCE', 20, '3rd', 'abcde@gmail.com', 2, '083674d4e835647f74ee7239348b7676', 0, 'student'),
(11, 31409, 'Sky', '', 'Allado', '1111-11-11', 'Lapaz', 'Iloilo', 'Iloilo', 'BSEE', 20, '1st', 'lkjfgvkjbh@gmail.com', 5, '6ed91de200500596fc39e67aae488899', 0, 'student'),
(12, 31410, 'Cherry', '', 'Monocay', '7524-11-25', 'Lapaz', 'Iloilo', 'Iloilo', 'BSIT', 20, '4th', 'asdad@email.com', 1, 'bc17bb01ce324199a26d171725b88407', 0, 'student'),
(13, 31412, 'Keah', '', 'Acollador', '1998-12-11', 'Lapaz', 'Iloilo', 'Iloilo', 'BSCE', 19, '3th', 'keah@email.com', 2, 'b74f4e53308fc1002d7b340cb2ecd1fd', 0, 'student'),
(14, 31413, 'Bryan', '', 'Caoyonan', '1990-02-11', 'Lapaz', 'Iloilo', 'Iloilo', 'BSEE', 27, '4th', 'brycaoyonan@email.com', 5, 'b0bca026ec3d57da6cfaa60c887344fd', 0, 'student'),
(15, 31414, 'Daniel', '', 'Deocadez', '1995-06-23', 'Jaro', 'Iloilo', 'Iloilo', 'BSCompE', 22, '4th', 'dan2@email.com', 6, 'e8cce457223e233ca7e27a9fca173da8', 0, 'student'),
(16, 31415, 'Gayle', '', 'Sobusa', '1996-07-11', 'Jaro', 'Iloilo', 'Iloilo', 'BSCompE', 21, '4th', 'Gayle_12@email.com', 6, '6c2592b41c6cb0a359137c9efd0448e3', 0, 'student'),
(17, 31416, 'Ronielle', '', 'Feller', '1996-03-11', 'Lapaz', 'Iloilo', 'Iloilo', 'BSME', 21, '4th', 'feller@email.com', 3, '3dd4da529f398d705c070f753c1bd4eb', 0, 'student'),
(18, 31417, 'John Michael', '', 'Yap', '1997-03-26', 'Lapaz', 'Iloilo', 'Iloilo', 'BSME', 20, '4th', 'Mich@email.com', 3, 'a23fac0cf4370365385023c45dbd21ca', 0, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `department` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `code`, `department`) VALUES
(7, 'IT421', 1),
(8, 'CE421', 2),
(9, 'EE421', 5),
(10, 'ME421', 3),
(11, 'CompE421', 6),
(12, 'IT411', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `firstname`, `middlename`, `lastname`, `department`) VALUES
(7, 'Arnold', '', 'Narte', '1'),
(8, 'albert', '', 'Aligarbes', '5'),
(9, 'Daray', '', 'Balasoto', '2'),
(10, 'Joel', '', 'Lopez', '3'),
(11, 'Diego', '', 'Lasanto', '3'),
(12, 'Ricarte', '', 'Nunez', '3'),
(13, 'Eduardo', '', 'Salavania', '3'),
(14, 'Juan', '', 'Ituralde', '3'),
(15, 'Gerald Glenn', '', 'Dalisay', '6'),
(16, 'John Rey', '', 'Alipe', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `school_year` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `department` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `school_year`, `semester`, `department`, `teacher`, `subject`, `title`, `student_id`) VALUES
(22, '2009-2013', '1st Semester', 1, 5, 2, '', 1),
(26, '2016-2017', '2nd Semester', 1, 7, 7, '', 9),
(27, '2016-2017', '2nd Semester', 1, 7, 7, '', 12),
(28, '2016-2017', '2nd Semester', 2, 9, 8, '', 13),
(29, '2016-2017', '2nd Semester', 2, 9, 8, '', 10),
(30, '2016-2017', '2nd Semester', 5, 8, 9, '', 11),
(31, '2016-2017', '2nd Semester', 5, 8, 9, '', 14),
(32, '2016-2017', '2nd Semester', 3, 10, 10, '', 17),
(33, '2016-2017', '2nd Semester', 3, 10, 10, '', 18),
(34, '2016-2017', '2nd Semester', 6, 15, 11, '', 15),
(35, '2016-2017', '2nd Semester', 6, 15, 11, '', 16),
(36, '2016-2017', '1st Semester', 1, 16, 12, 'Multimedia', 9),
(37, '2016-2017', '1st Semester', 1, 16, 12, 'Multimedia', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment`
--
ALTER TABLE `assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assessment`
--
ALTER TABLE `assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
