<!DOCTYPE html>
<html>
  <head>
    <title>WIT Evaluation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=BASEPATH?>public/assets/bootstrap/css/bootstrap.min.css?<?=mt_rand();?>" rel="stylesheet">
    <!-- styles -->
    <link href="<?=BASEPATH?>public/assets/css/styles.css?<?=mt_rand();?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="<?=BASEPATH;?>">WIT Evaluation System</a></h1>
	              </div>
	              <div class="pull-right main-navbar" style="margin-top: -35px; color: #fff;">
	              	<ul>
	              		<li><a href="<?=BASEPATH?>">Home</a></li>
	              		<li><a href="<?=BASEPATH?>about_us">About Us</a></li>
	              		<li><a href="<?=BASEPATH?>login">Login</a></li>
	              		<li><a href="<?=BASEPATH?>register">Register</a></li>
	              	</ul>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="loading"></div>
						<div class="col-md-3"></div>
						<div class="col-md-6">
				            <form id="form-add-user" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','register/add','form-add-user'); return false;">

				              <div class="form-group">
				                <label for="stud_number" class="col-sm-2 control-label">Student Number</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="stud_number" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="password" class="col-sm-2 control-label">Password</label>
				                  <div class="col-sm-10">
				                    <input type="password" class="form-control" name="password" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="firstname" class="col-sm-2 control-label">First Name</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="firstname" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="middlename" class="col-sm-2 control-label">Middle Name</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="middlename" >
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="lastname" class="col-sm-2 control-label">Last Name</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="lastname" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="street_brgy" class="col-sm-2 control-label">Street / Barangay</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="street_brgy" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="city" class="col-sm-2 control-label">City</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="city" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="province" class="col-sm-2 control-label">Province</label>
				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" name="province" required>
				                  </div>
				              </div>

				              <div class="form-group">
				                <label for="department" class="col-sm-2 control-label">Department</label>
				                  <div class="col-sm-10">
				                    <select name="department" class="form-control">
				                      <option value="-">-</option>
				                      <option value="College of Engineering">College of Engineering</option>
				                      <option value="College of Education">College of Education</option>
				                      <option value="College of Arts and Sciences">College of Arts and Sciences</option>
				                    </select>
				                  </div>
				              </div>              

				              <button class="btn btn-primary pull-right">Submit</button>
				            </form>							
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASEPATH;?>public/assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=BASEPATH?>public/assets/bootstrap/js/bootstrap.min.js?<?=mt_rand();?>"></script>
    <script src="<?=BASEPATH?>public/assets/js/custom.js?<?=mt_rand();?>"></script>
	<script src="<?=BASEPATH;?>public/assets/js/main_js.js?<?php echo mt_rand();?>"></script>
  </body>
</html>