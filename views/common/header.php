<!DOCTYPE html>
<html>
  <head>
    <title>WIT Evaluation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=BASEPATH;?>public/assets/bootstrap/css/bootstrap.min.css?<?php echo mt_rand();?>" rel="stylesheet">
    <!-- styles -->
    <link href="<?=BASEPATH;?>public/assets/css/styles.css?<?php echo mt_rand();?>" rel="stylesheet">

    <link href="<?=BASEPATH;?>public/assets/vendors/datatables/dataTables.bootstrap.css?<?php echo mt_rand();?>" rel="stylesheet" media="screen">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="<?=BASEPATH;?>dashboard">WIT Evaluation System</a></h1>
	              </div>
	           </div>
	           <div class="col-md-2 pull-right">
                <h5 style="color: #fff;padding: 10px 0px;">Welcome <?php echo $_SESSION['user']['lastname'].", ".$_SESSION['user']['firstname']; ?></h5>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
      <div class="loading"></div>
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <?php if ($_SESSION['user']['type'] == "admin") : ?>
                    <!--li><a href="<?=BASEPATH;?>dashboard"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li-->
                    <li><a href="<?=BASEPATH;?>users"><i class="glyphicon glyphicon-user"></i> Users</a></li>
                    <li><a href="<?=BASEPATH;?>teachers"><i class="glyphicon glyphicon-list"></i> Teachers</a></li>
                    <!--li><a href="<?=BASEPATH;?>evaluations"><i class="glyphicon glyphicon-star"></i> Evaluations</a></li-->
                    <li><a href="<?=BASEPATH;?>deptsubject"><i class="glyphicon glyphicon-list"></i> Department & Subject</a></li>
                    <li><a href="<?=BASEPATH;?>logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                    <?php elseif ($_SESSION['user']['type'] == "student") : ?>
                    <li><a href="<?=BASEPATH;?>profiles"><i class="glyphicon glyphicon-home"></i> Profiles</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-star"></i> Assessment</a></li>
                    <li><a href="<?=BASEPATH;?>teachers"><i class="glyphicon glyphicon-list"></i> Teachers</a></li>
                    <li><a href="<?=BASEPATH;?>logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                    <?php endif; ?>
                </ul>
             </div>
		  </div>