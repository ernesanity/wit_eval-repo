`<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Users</h4>
      </div>
      <div class="modal-body">
        <div class="content-box-large">
          <div class="panel-body">
            <form id="form-add-user" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>users/add','form-add-user'); return false;">

              <div class="col-md-6">

                <div class="form-group">
                  <label for="firstname" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="firstname" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="middlename" class="col-sm-2 control-label">Middle Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="middlename" >
                    </div>
                </div>

                <div class="form-group">
                  <label for="lastname" class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="lastname" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" name="dob" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="street_brgy" class="col-sm-2 control-label">Street / Barangay</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="street_brgy" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="city" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="city" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="province" class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="province" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="age" class="col-sm-2 control-label">Age</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" min="1" name="age" required>
                    </div>
                </div>                

              </div>

              <div class="col-md-6">

                <div class="form-group">
                  <label for="student_num" class="col-sm-2 control-label">Student No.</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="student_num" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="course" class="col-sm-2 control-label">Course</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="course" required>
                    </div>
                </div>

                <div class="form-group">
                  <label for="year" class="col-sm-2 control-label">Year</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="year" required>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-10">
                      <select name="department" class="form-control">
                        <option value="-">-</option>

                        <?php
                          $dept = getDept();

                          foreach ($dept as $department) :

                            echo '<option value="'.$department['id'].'">'.$department['dept_name'].'</option>';

                          endforeach;
                        ?>

                      </select>
                    </div>
                </div>

                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email Address</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" name="email" required>
                    </div>
                </div> 

                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="password" name="password" style="width: 54%;display: inline-block;margin-right: 15px;" readonly />
                      <button class="btn btn-primary btn-xs" style="display: inline-block;" onclick="AjaxObject.startRequest('post','users/generatePass',''); return false;">Generate Password</button>
                    </div>
                </div>  

              </div>             

              <div class="clearfix"></div>

              <div class="form-group pull-right">
                <button class="btn btn-primary ">Submit</button>
              </div>
            </form>
          </div>
        </div>        
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Student</h4>
      </div>
      <div class="modal-body">     
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-teacher-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Teachers</h4>
      </div>
      <div class="modal-body">
        <div class="content-box-large">
          <div class="panel-body">
            <form id="form-add-teacher" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','teachers/add','form-add-teacher'); return false;">

              <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label">First Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="firstname" required>
                  </div>
              </div>

              <div class="form-group">
                <label for="middlename" class="col-sm-2 control-label">Middle Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="middlename" >
                  </div>
              </div>

              <div class="form-group">
                <label for="lastname" class="col-sm-2 control-label">Last Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="lastname" required>
                  </div>
              </div>

                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-10">
                      <select name="department" class="form-control">
                        <option value="-">-</option>

                        <?php
                          $dept = getDept();

                          foreach ($dept as $department) :

                            echo '<option value="'.$department['id'].'">'.$department['dept_name'].'</option>';

                          endforeach;
                        ?>

                      </select>
                    </div>
                </div>             

              <button class="btn btn-primary ">Submit</button>
            </form>
          </div>
        </div>        
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editTeacher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Teacher</h4>
      </div>
      <div class="modal-body">     
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-user-notif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Users</h4>
      </div>
      <div class="modal-body">       
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="stud-assessment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Student Assessment</h4>
      </div>
      <div class="modal-body">

            <form id="form-assessment" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','dashboard/assessment','form-assessment'); return false;">

              <input type="hidden" name="u_id" value="<?=$_SESSION['user']['id']?>">

              <div class="form-group">
                <label for="q1" class="col-sm-2 control-label">1 + 1 = ?</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="q1" required>
                  </div>
              </div>

              <div class="form-group">
                <label for="q2" class="col-sm-2 control-label">2 + 2 = ?</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="q2" required>
                  </div>
              </div>

              <div class="form-group">
                <label for="q3" class="col-sm-2 control-label">3 + 3 = ?</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="q3" required>
                  </div>
              </div>

              <div class="form-group">
                <label for="q4" class="col-sm-2 control-label">4 + 4 = ?</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="q4" required>
                  </div>
              </div>

              <div class="form-group">
                <label for="q5" class="col-sm-2 control-label">5 + 5 = ?</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="q5" required>
                  </div>
              </div>             

              <button class="btn btn-primary ">Submit</button>
            </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-subject-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Subject</h4>
      </div>
      <div class="modal-body">

        <form id="form-add-subject" method="POST" action="" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>deptsubject/addSubject','form-add-subject'); return false;">

          <div class="form-group">
            <label>Subject Code :</label>
            <input type="text" name="code" class="form-control" required>
          </div>

          <div class="form-group">
            <label>Department :</label>
            <select class="form-control" name="department">
              <option value="-">-</option>
              <?php $departments = getDept(); ?>
              <?php foreach ($departments as $department) : ?>
                <option value="<?=$department['id']?>"><?=$department['dept_name']?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="form-group">
            <button class="btn btn-info btn-sm">Submit</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-department-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Department</h4>
      </div>
      <div class="modal-body">

        <form id="form-add-department" method="POST" action="" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>deptsubject/addDepartment','form-add-department'); return false;">

          <div class="form-group">
            <label>Department Name :</label>
            <input type="text" name="dept_name" class="form-control" required>
          </div>

          <div class="form-group">
            <button class="btn btn-info btn-sm">Submit</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="teacher-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Comments</h4>
      </div>
      <div class="modal-body">     
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-notif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Comments</h4>
      </div-->
      <div class="modal-body">     
      </div>
    </div>
  </div>
</div>