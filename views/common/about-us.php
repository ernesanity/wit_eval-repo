<!DOCTYPE html>
<html>
  <head>
    <title>WIT Evaluation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=BASEPATH?>public/assets/bootstrap/css/bootstrap.min.css?<?=mt_rand();?>" rel="stylesheet">
    <!-- styles -->
    <link href="<?=BASEPATH?>public/assets/css/styles.css?<?=mt_rand();?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="<?=BASEPATH;?>">WIT Evaluation System</a></h1>
	              </div>
	              <div class="pull-right main-navbar" style="margin-top: -35px; color: #fff;">
	              	<ul>
	              		<li><a href="<?=BASEPATH?>">Home</a></li>
	              		<li><a href="<?=BASEPATH?>about_us">About Us</a></li>
	              		<li><a href="<?=BASEPATH?>login">Login</a></li>
	              		<li><a href="<?=BASEPATH?>register">Register</a></li>
	              	</ul>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="content-box-large">
							<div class="panel-heading">
							<div class="panel-title">About Us</div>
						</div>
							<div class="panel-body">
								Ut tristique adipiscing mauris, sit amet suscipit metus porta quis. Donec dictum tincidunt erat, eu blandit ligula. Nam sit amet dolor sapien. Quisque velit erat, congue sed suscipit vel, feugiat sit amet enim. Suspendisse interdum enim at mi tempor commodo. Sed tincidunt sed tortor eu scelerisque. Donec luctus malesuada vulputate. Nunc vel auctor metus, vel adipiscing odio. Aliquam aliquet rhoncus libero, at varius nisi pulvinar nec. Aliquam erat volutpat. Donec ut neque mi. Praesent enim nisl, bibendum vitae ante et, placerat pharetra magna. Donec facilisis nisl turpis, eget facilisis turpis semper non. Maecenas luctus ligula tincidunt iasdsd vitae ante et, 
				  			<br /><br />
				  			Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque sed consectetur erat. Maecenas in elementum libero. Sed consequat pellentesque ultricies. Ut laoreet vehicula nisl sed placerat. Duis posuere lectus n, eros et hendrerit pellentesque, ante magna condimentum sapien, eget ultrices eros libero non orci. Etiam varius diam lectus.
							<br /><br />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASEPATH;?>public/assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=BASEPATH?>public/assets/bootstrap/js/bootstrap.min.js?<?=mt_rand();?>"></script>
    <script src="<?=BASEPATH?>public/assets/js/custom.js?<?=mt_rand();?>"></script>
	<script src="<?=BASEPATH;?>public/assets/js/main_js.js?<?php echo mt_rand();?>"></script>
  </body>
</html>