		</div>
    </div>
    <input type="hidden" id="user_type" name="user_type" value="<?=$_SESSION['user']['type']?>" />
    <?php if ($_SESSION['user']['type'] == "student") : ?>
    <input type="hidden" id="firsttime_login" name="firsttime_login" value="<?=checkAssessment($_SESSION['user']['id'])?>" />
    <?php endif; ?>
    <?php include 'modals.php'; ?>
    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
         </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASEPATH;?>public/assets/js/jquery.js?<?php echo mt_rand();?>"></script>
    <script src="<?=BASEPATH;?>public/assets/js/jquery-ui.js?<?php echo mt_rand();?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="<?=BASEPATH;?>public/assets/vendors/datatables/js/jquery.dataTables.min.js?<?php echo mt_rand();?>"></script>

    <script src="<?=BASEPATH;?>public/assets/vendors/datatables/dataTables.bootstrap.js?<?php echo mt_rand();?>"></script>

    <script src="<?=BASEPATH;?>public/assets/bootstrap/js/bootstrap.min.js?<?php echo mt_rand();?>"></script>
    <script src="<?=BASEPATH;?>public/assets/js/custom.js?<?php echo mt_rand();?>"></script>
    <script src="<?=BASEPATH;?>public/assets/js/main_js.js?<?php echo mt_rand();?>"></script>
    <script src="<?=BASEPATH;?>public/assets/js/script.js?<?php echo mt_rand();?>"></script>
  </body>
</html>