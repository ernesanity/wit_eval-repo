<!DOCTYPE html>
<html>
  <head>
    <title>WIT Evaluation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=BASEPATH?>public/assets/bootstrap/css/bootstrap.min.css?<?=mt_rand();?>" rel="stylesheet">
    <!-- styles -->
    <link href="<?=BASEPATH?>public/assets/css/styles.css?<?=mt_rand();?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="<?=BASEPATH;?>">WIT Evaluation System</a></h1>
	              </div>
	              <!--div class="pull-right main-navbar" style="margin-top: -35px; color: #fff;">
	              	<ul>
	              		<li><a href="<?=BASEPATH?>">Home</a></li>
	              		<li><a href="<?=BASEPATH?>about_us">About Us</a></li>
	              		<li><a href="<?=BASEPATH?>login">Login</a></li>
	              		<li><a href="<?=BASEPATH?>register">Register</a></li>
	              	</ul>
	              </div-->
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="loading"></div>
			<div class="col-md-4 col-md-offset-4">
				<form id="form-login" method="POST" action="" role="form" onsubmit="AjaxObject.startRequest('post','login/login','form-login'); return false;">
					<div class="login-wrapper">
				        <div class="box">
				            <div class="content-wrap">
				                <h6>Sign In</h6>
				                <input class="form-control" name="username" type="text" placeholder="Username" required>
				                <input class="form-control" name="password" type="password" placeholder="Password" required>
				                <div class="action">
				                    <button class="btn btn-primary signup">Login</button>
				                </div>                
				            </div>
				        </div>

				        <div class="already">
				            <p>Don't have an account yet?</p>
				            <p>Please contact Administrator.</p>
				        </div>
				    </div>
			   	</form>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASEPATH;?>public/assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=BASEPATH?>public/assets/bootstrap/js/bootstrap.min.js?<?=mt_rand();?>"></script>
    <script src="<?=BASEPATH?>public/assets/js/custom.js?<?=mt_rand();?>"></script>
	<script src="<?=BASEPATH;?>public/assets/js/main_js.js?<?php echo mt_rand();?>"></script>
  </body>
</html>