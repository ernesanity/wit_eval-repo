<div class="col-md-10">
	<div class="row">
  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Users</div>
				</div>
				<div class="pull-right">
					<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus-sign"></i> New User</button>
				</div>
  				<div class="panel-body">
  					<div class="col-md-4">

  						<form id="form-search-user" method="POST" action="" class="form-horizontal" role="form" onsubmit="searchData('<?=BASEPATH?>users/search','form-search-user','user-table'); return false;">

	  						<div style="width:50%; float: left; padding-right: 10px;">
	  								<div class="form-group">
			  							<label>Department : </label>
			  							<select class="form-control" name="department">
			  								<option value="-">-</option>
			  								<?php foreach ($departments as $department) : ?>
			  									<option value="<?=$department['id']?>"><?=$department['dept_name']?></option>
			  								<?php endforeach; ?>
			  							</select>
									</div>							
	  						</div>

	  						<div style="width:40%; float: right;">

	  								<div class="form-group">
			  							<label>Search : </label>
			  							<input type="text" class="form-control" name="keyword">
									</div>								
	  						</div>

	  						<div class="clearfix"></div>

							<div class="form-group">
	  							<input type="submit" class="btn btn-info btn-xs" value="Search">
							</div>	
  						</form>

  						<div id="user-list">

		  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="user-table">
								<thead>
									<tr>
										<th>Name</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($users as $user) : ?>
										<tr>
											<td><?=$user['lastname']?>, <?=$user['firstname']?> <?=$user['middlename']?></td>
											<td><button class="btn btn-info btn-xs" onclick="addToList(<?=$user['id']?>, '<?=$user['lastname']?>, <?=$user['firstname']?> <?=$user['middlename']?>'); return false;">Add to List</button></td>
											<td><button class="btn btn-info btn-xs" onclick="editStudent(<?=$user['id']?>)"><i class="glyphicon glyphicon-pencil"></i></button></td>
											<td><button class="btn btn-danger btn-xs" onclick="deleteUser(<?=$user['id']?>);"><i class="glyphicon glyphicon-trash"></i></button></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

						</div>
					</div>

					<div class="col-md-8 pull-right">
						
						<div style="border: 1px solid #ccc;padding: 10px;border-radius: 0.3em;">
							
							<form id="form-user-details" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>users/addUser','form-user-details'); return false;">
								
								<div class="col-md-6">

									<div class="form-group">
										<label>School Year : </label>
										<input type="text" name="school_year" class="form-control" style="width: 60%;display: inline-block;" required />
									</div>

									<div class="form-group">
										<label>Semester : </label>
										<!--input type="text" name="school_year" class="form-control" style="width: 60%;display: inline-block;" required /-->
										<select name="semester" class="form-control" style="width: 60%;display: inline-block;">
											<option value="-">-</option>
											<option value="1st Semester">1st Semester</option>
											<option value="2nd Semester">2nd Semester</option>
											<option value="3rd Semester">3rd Semester</option>
											<option value="4th Semester">4th Semester</option>
										</select>
									</div>

	  								<div class="form-group">
			  							<label>Department : </label>
			  							<select class="form-control" name="department" style="width: 60%;display: inline-block;" onchange="getSubjectByDepartment(this.value); getTeacherByDepartment(this.value);">
			  								<option value="-">-</option>
			  								<?php foreach ($departments as $department) : ?>
			  									<option value="<?=$department['id']?>"><?=$department['dept_name']?></option>
			  								<?php endforeach; ?>
			  							</select>
									</div>

									<hr />

									<div id="students-list-wrapper">
										<h5>Student List</h5>

										<div id="students-list-hidden">
											<input type="hidden" name="stud_default" id="stud_default" value="1" />
										</div>
										<div id="students-list"></div>

									</div>	

								</div>

								<div class="col-md-6 pull-right">

									<div class="form-group">
										<label>Teacher : </label>
										<!--input type="text" name="school_year" class="form-control" style="width: 60%;display: inline-block;" required /-->
										<select name="teacher" id="teachers" class="form-control" style="width: 60%;display: inline-block;" disabled>
			  								<option value="-">-</option>
										</select>
									</div>

	  								<div class="form-group">
			  							<label>Subject Code : </label>
			  							<select class="form-control" name="subject" id="subjects" style="width: 60%;display: inline-block;" disabled>
			  								<option value="-">-</option>
			  							</select>
									</div>

									<div class="form-group">
										<label>Title : </label>
										<input type="text" name="title" class="form-control" style="width: 60%;display: inline-block;" />
									</div>	

								</div>

								<div class="clearfix"></div>


								<div class="form-group pull-right" style="padding-right: 20px;">
									<button class="btn btn-primary" onclick="filterClassList('<?=BASEPATH?>users/filter_class_list','form-user-details'); return false;">Filter Class List</button>
									<button class="btn btn-info">Save</button>
								</div>

							</form>

							<div class="clearfix"></div>

						</div>

					</div>
  				</div>
  			</div>
	</div>
</div>