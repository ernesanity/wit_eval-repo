<?php foreach ($students as $student) : ?>
<div class="content-box-large">
  <div class="panel-body">
    <form id="form-edit-user" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>users/edit','form-edit-user'); return false;">
      <input type="hidden" name="id" value="<?=$student['id']?>">
      <div class="col-md-6">

        <div class="form-group">
          <label for="firstname" class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="firstname" value="<?=$student['firstname']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="middlename" class="col-sm-2 control-label">Middle Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="middlename" value="<?=$student['middlename']?>" >
            </div>
        </div>

        <div class="form-group">
          <label for="lastname" class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="lastname" value="<?=$student['lastname']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" name="dob" value="<?=$student['dob']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="street_brgy" class="col-sm-2 control-label">Street / Barangay</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="street_brgy" value="<?=$student['st_brgy']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="city" class="col-sm-2 control-label">City</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="city" value="<?=$student['city']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="province" class="col-sm-2 control-label">Province</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="province" value="<?=$student['province']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="course" class="col-sm-2 control-label">Course</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="course" value="<?=$student['course']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="age" class="col-sm-2 control-label">Age</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" min="1" name="age" value="<?=$student['age']?>" required>
            </div>
        </div>                

      </div>

      <div class="col-md-6">

        <div class="form-group">
          <label for="student_num" class="col-sm-2 control-label">Student No.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="student_num" value="<?=$student['stud_num']?>" required>
            </div>
        </div>

        <div class="form-group">
          <label for="year" class="col-sm-2 control-label">Year</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="year" value="<?=$student['year']?>" required>
            </div>
        </div>
        
        <div class="form-group">
          <label for="department" class="col-sm-2 control-label">Department</label>
            <div class="col-sm-10">
              <select name="department" class="form-control">
                <option value="<?=$student['department']?>"><?=getDepartmentName($student['department'])?></option>
                <option value="-">-</option>

                <?php
                  $dept = getDept();

                  foreach ($dept as $department) :

                    echo '<option value="'.$department['id'].'">'.$department['dept_name'].'</option>';

                  endforeach;
                ?>

              </select>
            </div>
        </div>

        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email Address</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" name="email" value="<?=$student['email']?>" required>
            </div>
        </div>  

      </div>             

      <div class="clearfix"></div>

      <div class="form-group pull-right">
        <button class="btn btn-primary ">Submit</button>
      </div>
    </form>
  </div>
</div>
<?php endforeach; ?>   