<?php foreach ($users as $user) : ?>
	<tr>
		<td><?=$user['lastname']?>, <?=$user['firstname']?> <?=$user['middlename']?></td>
		<td><button class="btn btn-info btn-xs" onclick="addToList(<?=$user['id']?>, '<?=$user['lastname']?>, <?=$user['firstname']?> <?=$user['middlename']?>'); return false;">Add to List</button></td>
		<td><button class="btn btn-info btn-xs" onclick="editStudent(<?=$user['id']?>)"><i class="glyphicon glyphicon-pencil"></i></button></td>
		<td><button class="btn btn-danger btn-xs" onclick="deleteUser(<?=$user['id']?>);"><i class="glyphicon glyphicon-trash"></i></button></td>
	</tr>
<?php endforeach; ?>