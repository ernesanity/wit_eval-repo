<?php if (is_array($teachers)) : ?>
	<?php foreach ($teachers as $teacher) : ?>
		<option value="<?=$teacher['id']?>"><?=$teacher['lastname']?>, <?=$teacher['firstname']?> <?=$teacher['middlename']?></option>
	<?php endforeach; ?>
<?php else : ?>
	<option value="-">No teachers under this department.</option>
<?php endif; ?>