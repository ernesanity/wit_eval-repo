<?php if (is_array($subjects)) : ?>
	<?php foreach ($subjects as $subject) : ?>
		<option value="<?=$subject['id']?>"><?=$subject['code']?></option>
	<?php endforeach; ?>
<?php else : ?>
	<option value="-">No subjects under this department.</option>
<?php endif; ?>