<div class="col-md-10">
	<div class="row">
  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Evaluate</div>
				</div>
  				<div class="panel-body">
  					<form id="form-post-evaluate" method="POST" action="" role="form" onsubmit="AjaxObject.startRequest('post','<?=BASEPATH?>evaluations/postEvaluate','form-post-evaluate'); return false;">
  						<input type="hidden" name="student_id" value="<?=$_SESSION['user']['id']?>" />
              <input type="hidden" name="teacher_id" value="<?=$id?>" />
              <input type="hidden" name="department" value="<?=$_SESSION['user']['department']?>" />
              <input type="hidden" name="subject_id" value="<?=$subject_id?>" />
  						<input type="hidden" name="choices-hidden-val" value="1" />

              <div class="col-md-8 pull-left">

    						<table class="table table-bordered table-striped" id="evaluation-questions" style="margin-top: 50px;">
    							<tbody>
    								<tr>
    									<td height="50">1. Informs the students what topics to take-up during the semester based on the course outline, syllabus or Instructional Guide of the subject.</td>
    								</tr>

    								<tr>
    									<td>2. Follows strictly the subject matter in the course outline, syllabus or Instructional Guide and limits himself/herself to these topics.</td>
    								</tr>

    								<tr>
    									<td>3. Explains the lesson very clearly. (Using examples related to everyday life, which you can actually see or experience.)</td>
    								</tr>

    								<tr>
    									<td>4. Encourages student participation (such as assignments, seat work, recitation, asking questions and others.)</td>
    								</tr>

    								<tr>
    									<td>5. Attends and hold class the entire period. (Does not sleep, rest, read nor work on materials not related to subject. Does not work on his/her class records during the class. Does not entertain visitors, calls, or text messages.)</td>
    								</tr>

    								<tr>
    									<td>6. Begins class on time, based on school's standard time. (There is a 5-minute allowance to transfer from one room to the other.)</td>
    								</tr>

    								<tr>
    									<td>7. Ends class on time.</td>
    								</tr>

    								<tr>
    									<td>8. Is effective in the use of English as the language of instruction (or Filipino as required by the subject.)</td>
    								</tr>

    								<tr>
    									<td>9. Corrects/shows/returns test papers, assignments, seatwork, term papers, reports, plates, experiments, projects, etc.</td>
    								</tr>

    								<tr>
    									<td>10. Is fair in grading student performance. (No favoritism.)</td>
    								</tr>

                    <tr>
                      <td>11. Maintains discipline in the classroom. (No insulting, vulgar language, or shouting. Does not easily get angry. Encourages cleanliness like picking up of papers scattered on the floor. No spitting, No smoking. The class is well behaved.)</td>
                    </tr>

                    <tr>
                      <td>12. Entertains problems of the students related to the subject inside or outside class hours within school premises.</td>
                    </tr>

                    <tr>
                      <td>13. Strictly prevents cheating.</td>
                    </tr>

                    <tr>
                      <td>14. Enforces and practices school policies porperly and strictly like wearing of school ID and proper attire. (This also includes examination period.)</td>
                    </tr>

                    <tr>
                      <td>15. Avoids soliciting donations or loans in cash or in kind for personal use.</td>
                    </tr>

                    <tr>
                      <td>16. Avoids selling handouts, notes, pad paper, bond paper, folder, or other items to the students or through stores outside the School Canteen. (Photocopy of quiz questionnaire should be at cost, not more than 50&cent; per one (1) whole page. In another piece of paper, specify items being sold.)</td>
                    </tr>

    							</tbody>
    						</table>

              </div>

              <div class="col-md-4 pull-right">

                <table class="table table-bordered table-striped" id="evaluation-choices">

                  <thead>
                    <th>Never</th>
                    <th>Rarely</th>
                    <th>Sometimes</th>
                    <th>Most of the time</th>
                    <th>All the time</th>
                  </thead>

                  <tbody>
                    <?php

                      for ($i=1;$i<=16;$i++) :
                        if ($i == "3") $height = "37";
                        elseif ($i == "4") $height = "37";
                        elseif ($i == "7") $height = "37";
                        elseif ($i == "8") $height = "37";
                        elseif ($i == "9") $height = "37";
                        elseif ($i == "10") $height = "37";
                        elseif ($i == "11") $height = "80";
                        elseif ($i == "12") $height = "37";
                        elseif ($i == "13") $height = "37";
                        elseif ($i == "15") $height = "37";
                        elseif ($i == "16") $height = "78";
                        else $height = "56";
                    ?>
                      <tr>
                        <td height="<?=$height?>"><input type="radio" value="1" name="q<?=$i?>" onclick="clearHiddenVal();"></td>
                        <td><input type="radio" value="2" name="q<?=$i?>"></td>
                        <td><input type="radio" value="3" name="q<?=$i?>"></td>
                        <td><input type="radio" value="4" name="q<?=$i?>"></td>
                        <td><input type="radio" value="5" name="q<?=$i?>"></td>
                      </tr>
                    <?php                        
                      endfor;

                    ?>
                  </tbody>
                </table>

              </div>

              <div class="clearfix"></div>

              <h5><b>Additional student comments</b></h5>

              <div class="form-group">
                <label>1. What are the strong points of your teacher?</label>
                <textarea class="form-control" name="strong_points"></textarea>
              </div>

              <div class="form-group">
                <label>2. How may your teacher improve his/her teaching?</label>
                <textarea class="form-control" name="improve_teaching"></textarea>
              </div>

              <div class="form-group">
                <label>3. Other comments.</label>
                <textarea class="form-control" name="other"></textarea>
              </div>

  						<div class="pull-right">
  							<button class="btn btn-info btn-sm">Submit</button>
  						</div>
  					</form>
  				</div>
  			</div>
	</div>
</div>