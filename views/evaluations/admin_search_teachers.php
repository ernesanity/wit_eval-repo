<?php if (is_array($teachers)) : ?>

		<?php

		foreach ($teachers as $teacher) :
				# code...
			$teach = getTeacher($teacher['teacher']);

			foreach ($teach as $t) :
		?>

		<tr>
			<td><?=$t['lastname']?>, <?=$t['firstname']?> <?=$t['middlename']?></td>
			<td><?=getDepartmentName($t['department'])?></td>
			<td><?=getTeacherRating($t['id'])?>%</td>
		</tr>

		<?php
			endforeach;
		endforeach;
		?>

		<tr>
			
		</tr>
<?php else : ?>
	<tr><td colspan="4"><div class="alert alert-danger">No teacher found.</div></td></tr>
<?php endif; ?>