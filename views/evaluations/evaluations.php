<div class="col-md-10">
	<div class="row">
  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Evaluations</div>
				</div>
  				<div class="panel-body">
  					<div class="col-md-4">
              <?php
                if ($_SESSION['user']['type'] == "student") {
                  $url = BASEPATH.'teachers/getTeachers';
                } else {
                  $url = BASEPATH.'evaluations/getTeachersv2';
                }
              ?>
  						<form id="form-get-teacher" method="POST" action="" class="form-horizontal" role="form" onsubmit="searchData('<?=$url?>','form-get-teacher', 'teacher-table'); return false;">
  							<div class="form-group">
  								<label>Department : </label>
  								<span><?=getDepartmentName($_SESSION['user']['department'])?></span>
  								<input type="hidden" name="department" value="<?=$_SESSION['user']['department']?>">
  							</div>

  							<div class="form-group">
  								<label>Teacher : </label>
  								<select class="form-control" style="width:50%;" name="teacher">
  									<option value="-">-</option>
									<?php foreach ($teachers as $teacher) : ?>
										<option value="<?=$teacher['id']?>"><?=$teacher['lastname']?>, <?=$teacher['firstname']?> <?=$teacher['middlename']?></option>
									<?php endforeach; ?>
  								</select>
  							</div>

  							<div class="form-group">
  								<label>Subject : </label>
  								<select class="form-control" style="width:50%;" name="subject">
  									<option value="-">-</option>
									<?php foreach ($subjects as $subject) : ?>
										<option value="<?=$subject['id']?>"><?=$subject['code']?></option>
									<?php endforeach; ?>
  								</select>
  							</div>

                <div class="form-group">
                  <label>Semester : </label>
                  <select class="form-control" style="width:50%;" name="semester">
                    <option value="-">-</option>
                    <option value="1st Semester">1st Semester</option>
                    <option value="2nd Semester">2nd Semester</option>
                  </select>
                </div>

  							<div class="form-group">
  								<button class="btn btn-info btn-sm">Submit</button>
  							</div>

  						</form>
  					</div>
  					<div class="col-md-8">
  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="teacher-table">
						<thead>
							<tr>
								<th>Name</th>
                <th>Subject</th>
                <th>Description</th>
                <th>Department</th>
								<th>Status</th>
								<?php if ($_SESSION['user']['type'] == "admin") : ?>
								<th>Rating</th>
								<?php endif; ?>
							</tr>
						</thead>
						<tbody>
							<tr><td colspan="5">Please select options</td></tr>
						</tbody>
					</table>
  					</div>
  				</div>
  			</div>
	</div>
</div>