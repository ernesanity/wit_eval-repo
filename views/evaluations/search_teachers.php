<?php foreach ($teachers as $teacher) : ?>
	<tr>
		<td><?=$teacher['lastname']?>, <?=$teacher['firstname']?> <?=$teacher['middlename']?></td>
		<td><?=getDepartmentName($teacher['department'])?></td>
		<td>
			<?php if (checkStudentEval($_SESSION['user']['id'], $teacher['id'])) :?>
				<button class="btn btn-danger btn-sm" disabled>You already evaluated this teacher.</button>
			<?php else : ?>
				<a href="<?=BASEPATH?>evaluations/evaluate/<?=$teacher['id']?>" class="btn btn-info btn-sm">Evaluate</a>
			<?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
