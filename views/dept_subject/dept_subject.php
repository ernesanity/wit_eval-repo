<div class="col-md-10">
	<div class="row">
  			<div class="content-box-large">
  				<div class="panel-heading">
					 <div class="panel-title">Department & Subject</div>
				  </div>
  				<div class="panel-body">

            <div class="col-md-5">
                
                <h4>Department</h4>  

                <br />

                <div class="pull-right">
                  <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-department-modal"><i class="glyphicon glyphicon-plus-sign"></i> New Department</button>
                </div>

                <br />

                <table class="table table-bordered table-striped" style="margin-top: 20px;">
                  <thead>
                    <th>Name</th>
                    <th></th>
                    <th></th>
                  </thead>

                  <tbody>
                    <?php foreach ($departments as $department) : ?>
                      <tr>
                        <td><?=$department['dept_name']?></td>
                        <td><button class="btn btn-info btn-xs" onclick=""><i class="glyphicon glyphicon-pencil"></i></button></td>
                        <td><button class="btn btn-danger btn-xs" onclick=""><i class="glyphicon glyphicon-trash"></i></button></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>

            </div>

            <div class="col-md-5">
              
              
                
                <h4>Subject</h4>  

                <br />

                <div class="pull-right">
                  <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-subject-modal"><i class="glyphicon glyphicon-plus-sign"></i> New Subject</button>
                </div>

                <br />

                <table class="table table-bordered table-striped" style="margin-top: 20px;">
                  <thead>
                    <th>Subject Code</th>
                    <th>Department</th>
                    <th></th>
                    <th></th>
                  </thead>

                  <tbody>
                    <?php foreach ($subjects as $subject) : ?>
                      <tr>
                        <td><?=$subject['code']?></td>
                        <td><?=getDepartmentName($subject['department'])?></td>
                        <td><button class="btn btn-info btn-xs" onclick=""><i class="glyphicon glyphicon-pencil"></i></button></td>
                        <td><button class="btn btn-danger btn-xs" onclick=""><i class="glyphicon glyphicon-trash"></i></button></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>

            </div>

  				</div>
  			</div>
	</div>
</div>