<div class="col-md-10">
	<div class="row">
		<div class="col-md-12">
			<div class="content-box-large">
				<div class="panel-heading">
				<div class="panel-title">Profile</div>
				
			</div>
				<div class="panel-body">
					<div class="col-md-6">
						<?php foreach ($profiles as $profile) : ?>
							<div class="form-group">
								<label>Name : </label>
								<span><?=$profile['lastname']?>, <?=$profile['firstname']?> <?=$profile['middlename']?></span>
							</div>

							<div class="form-group">
								<label>Department : </label>
								<span><?=getDepartmentName($profile['department'])?></span>
							</div>

							<div class="form-group">
								<label>Course : </label>
								<span><?=$profile['course']?></span>
							</div>

							<div class="form-group">
								<label>Year : </label>
								<span><?=$profile['year']?></span>
							</div>

							<div class="form-group">
								<label>Address : </label> <br />
								<span><b>Street / Baranggay</b> : <?=$profile['st_brgy']?> <br /> <b>City</b> : <?=$profile['city']?> <br /> <b>Province</b> : <?=$profile['province']?></span>
							</div>

							<div class="form-group">
								<label>Age  : </label>
								<span><?=$profile['age']?></span>
							</div>

							<div class="form-group">
								<label>Date of Birth : </label>
								<span><?=cleanDate($profile['dob'])?></span>
							</div>
						<?php endforeach; ?> 
					</div>

					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="content-box-header">
			  					<div class="panel-title">Instruction 1</div>
								
				  			</div>
				  			<div class="content-box-large box-with-header">
				  				
					  			Pellentesque luctus quam quis consequat vulputate. Sed sit amet diam ipsum. Praesent in pellentesque diam, sit amet dignissim erat. Aliquam erat volutpat. Aenean laoreet metus leo, laoreet feugiat enim suscipit quis. Praesent mauris mauris, ornare vitae tincidunt sed, hendrerit eget augue. Nam nec vestibulum nisi, eu dignissim nulla.
								<br /><br />
							</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="content-box-header">
			  					<div class="panel-title">Instruction 2</div>
								
				  			</div>
				  			<div class="content-box-large box-with-header">
				  				
					  			Pellentesque luctus quam quis consequat vulputate. Sed sit amet diam ipsum. Praesent in pellentesque diam, sit amet dignissim erat. Aliquam erat volutpat. Aenean laoreet metus leo, laoreet feugiat enim suscipit quis. Praesent mauris mauris, ornare vitae tincidunt sed, hendrerit eget augue. Nam nec vestibulum nisi, eu dignissim nulla.
								<br /><br />
							</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>

	</div>
</div>