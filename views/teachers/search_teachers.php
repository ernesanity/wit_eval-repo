<?php if (is_array($teachers)) : ?>
<?php foreach ($teachers as $teacher) : ?>
	<?php $id = $teacher['teacher']; $department = $teacher['department']; ?>
	<?php $teach = getTeacher($teacher['teacher']); ?>
		<?php foreach ($teach as $t) : ?>
			<div class="col-md-5 pull-left">
				<div class="form-group">
					<label>Name : <?=$t['lastname']?>, <?=$t['firstname']?> <?=$t['middlename']?></label>
				</div>

				<div class="form-group">
					<label>Department : <?=getDepartmentName($teacher['department'])?></label>
				</div>
			</div>

			<div class="col-md-5 pull-right">
				<div class="form-group">
					<label>Semester : <?=$teacher['semester']?></label>
				</div>

				<div class="form-group">
					<label>School Year : <?=$teacher['school_year']?></label>
				</div>
			</div>
		<?php endforeach; ?>
<?php endforeach; ?>

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="teacher-table">
	<thead>
		<tr>
			<th>Subject Code</th>
			<th>Department</th>
			<th>Status</th>
			<th>No. of Students Evaluated</th>
			<th>Comments</th>
			<th>Result</th>
		</tr>
	</thead>
	<tbody>

		<?php

		$subjects = getTeacherSubjects($id);

		foreach ($subjects as $subject) :
			$sub = getSubject($subject['subject']);

			foreach ($sub as $s) :
				# code...
			$status = teacherStatus($id, $department, $s['id']);
			$studentsEvaluatedTeacher = countStudentsEvaluated($id, $department, $s['id']);
		?>

		<tr>
			<td><?=$s['code']?></td>
			<td><?=getDepartmentName($s['department'])?></td>
			<td>
				<?php 
					if ($status == 0) echo "Not yet evaluated";
					else echo "Evaluated";
				?>
			</td>
			<td>
				<?=$status?>/<?=$studentsEvaluatedTeacher?>
			</td>
			<td><button class="btn btn-info btn-xs" onclick="getTeacherComments(<?=$id?>, <?=$department?>, <?=$s['id']?>); return false;">View Comments</button></td>
			<td><?=getTeacherRating($id)?>%</td>
		</tr>

		<?php

			endforeach;

		endforeach;
		?>

		<tr>
			
		</tr>
	</tbody>
</table>
<?php else : ?>
	<div class="alert alert-danger">No teacher found.</div>
<?php endif; ?>