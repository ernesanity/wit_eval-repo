<?php foreach ($teachers as $teacher) : ?>
<form id="form-edit-teacher" method="POST" action="" class="form-horizontal" role="form" onsubmit="AjaxObject.startRequest('post','teachers/edit','form-edit-teacher'); return false;">
  <input type="hidden" name="id" value="<?=$teacher['id']?>">
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">First Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="firstname" value="<?=$teacher['firstname']?>" required>
      </div>
  </div>

  <div class="form-group">
    <label for="middlename" class="col-sm-2 control-label">Middle Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="middlename" value="<?=$teacher['middlename']?>" >
      </div>
  </div>

  <div class="form-group">
    <label for="lastname" class="col-sm-2 control-label">Last Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="lastname" value="<?=$teacher['lastname']?>" required>
      </div>
  </div>

    <div class="form-group">
      <label for="department" class="col-sm-2 control-label">Department</label>
        <div class="col-sm-10">
          <select name="department" class="form-control">
            <option value="<?=$teacher['department']?>" selected><?=getDepartmentName($teacher['department'])?></option>
            <option value="-">-</option>

            <?php
              $dept = getDept();

              foreach ($dept as $department) :

                echo '<option value="'.$department['id'].'">'.$department['dept_name'].'</option>';

              endforeach;
            ?>

          </select>
        </div>
    </div>             

  <button class="btn btn-primary ">Submit</button>
</form>
<?php endforeach; ?>