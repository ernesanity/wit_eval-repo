<div class="col-md-10">
	<div class="row">
  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Teachers</div>
				</div>
				<div class="pull-right">
					<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-teacher-modal"><i class="glyphicon glyphicon-plus-sign"></i> New Teacher</button>
				</div>
  				<div class="panel-body">

  					<div class="col-md-4">

  						<form id="form-search-teachers" method="POST" action="" class="form-horizontal" role="form" onsubmit="searchTeacher('<?=BASEPATH?>teachers/search_teachers','form-search-teachers','teacher-wrapper'); return false;">

  						<div style="width:40%; float:left;">
							<div class="form-group">
	  							<label>School Year : </label>
	  							<input type="text" class="form-control" id="school_year" name="school_year" onblur="getTeachers($('#school_year').val(),$('#semester').val(),$('#department').val());">
							</div>

							<div class="form-group">
	  							<label>Semester : </label>
	  							<select class="form-control" id="semester" name="semester" onchange="getTeachers($('#school_year').val(),$('#semester').val(),$('#department').val());">
	  								<option value="-">-</option>
	  								<option value="1st Semester">1st Semester</option>
	  								<option value="2nd Semester">2nd Semester</option>
	  							</select>
							</div>

						</div>

						<div style="width:40%; float:right;">

							<div class="form-group">
	  							<label>Department : </label>
	  							<select class="form-control" id="department" name="department" onchange="getTeachers($('#school_year').val(),$('#semester').val(),$('#department').val());">
	  								<option value="-">-</option>
	  								<?php foreach ($departments as $department) : ?>
	  									<option value="<?=$department['id']?>"><?=$department['dept_name']?></option>
	  								<?php endforeach; ?>
	  							</select>
							</div>

							<div class="form-group">
	  							<label>Teacher : </label>
	  							<!--input type="text" class="form-control" name="teacher" disabled-->
	  							<select class="form-control" id="teacher" name="teacher" disabled>
	  								<option value="-">-</option>
	  							</select>
							</div>	
						
						</div>

						<div class="clearfix"></div>
	

	  						<div class="clearfix"></div>

							<div class="form-group">
	  							<input type="submit" class="btn btn-info btn-xs" value="Search">
							</div>	
  						</form>
					</div>

					<div class="col-md-8 pull-right" id="teacher-wrapper">

					</div>
  				</div>
  			</div>
	</div>
</div>