<?php
/**
 * Generic model make db connection or what you need
 *
 */
class Model {

	public $db;

	function __construct() {
		
		$database = new Database();
		$db = $database->dbConnect();
		$this->db = $db;		

	}

}
