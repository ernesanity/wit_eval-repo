<?php
/**
 *
 * basic view class  
 *
 */

class LoadView {

	private $properties;

	function __construct() {

	}
/**
 * 
 * Basic render function with attached header and footer
 * 
 * 
 */
	public function render($name, $data = null) {
		if (is_array($data)) {
			extract($data);
		}
		if (!empty($properties) && is_array($properties) ) {
			extract($properties);
		}

		$haystack = array("common/home","common/main", "common/about-us", "common/register", "users/edit_student", "users/teachers", "users/subjects", "teachers/edit_teacher", "teachers/search_teachers", "evaluations/search_teachers", "evaluations/admin_search_teachers", "users/search_users");

		if (in_array($name, $haystack)) {
			include VIEWPATH . $name . '.php';
		} else {
			include VIEWPATH . 'common/header.php';
			include VIEWPATH . $name . '.php';
			include VIEWPATH . 'common/footer.php';
		}

	}

	public function __set($property, $value) {
		if (!isset($this -> $property)) {
			$this -> properties[$property] = $value;
		}
	}

	public function __get($property) {
		if (isset($this -> properties[$property])) {
			return $this -> properties[$property];
		}
	}

}
