<?php
/**
 * Implement a db wrapper if needed
 * 
 */
class Database 
{
	public $db;
	public function __construct()
	{
		//$this->db=new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);		
		
	}

	public function dbConnect() 
	{
	    $this->db = null;    
        try
		{
            $this->db = new PDO(DB_TYPE.":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
        }
		catch(PDOException $exception)
		{
            echo "Connection error: " . $exception->getMessage();
        }
         
        return $this->db;		
	}
	
	/*
	function fetchAllAssoc($sql, $bindData=null) {

		$sth = $this -> db -> prepare($sql);

		$sth -> execute($bindData);
		
		return $sth -> fetchAll(PDO::FETCH_ASSOC);

	}

	function fetchSingle($sql, $bindData=null) {

		$sth = $this -> db -> prepare($sql);

		$sth -> execute($bindData);

		return $sth -> fetch();

	}

	public function onlyExecute($sql, $bindData=null) {
			
		$sth = $this -> db -> prepare($sql);

		return $sth -> execute($bindData);

	}
	*/
	 
}
