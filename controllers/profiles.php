<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Profiles extends Controller {

	function __construct() {

		
			parent::__construct('dashboard_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {

		$data['profiles'] = $this->model->getStudentData($_SESSION['user']['id']);
		$this->viewLoader->render('profiles/profiles', $data);

	}

	function assessment() {

		if ($this->model->assessment($_POST['q1'], $_POST['q2'], $_POST['q3'], $_POST['q4'], $_POST['q5'], $_POST['u_id'])) {
			$array['html'] = "<div class='alert alert-success'>Assessment is successfully submitted.</div>";
			$array['javascript'] = "setTimeout(function(){ $('#stud-assessment-modal').modal('hide'); },500);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Something went wrong while trying to submit assessment.</div>";			
		}

		echo json_encode($array);

	}


}
