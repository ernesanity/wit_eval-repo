<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Evaluations extends Controller {

	function __construct() {

		
			parent::__construct('evaluations_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {
		$data['teachers'] = $this->model->getTeachers($_SESSION['user']['department'], $_SESSION['user']['type']);
		$this->viewLoader->render('evaluations/evaluations', $data);
	}

	function evaluate($id) {

		$data['id'] = $id;
		$data['subject_id'] = $_GET['subject_id'];
		$this->viewLoader->render('evaluations/evaluate', $data);

	}

	function postEvaluate() {


		//if ($_POST['q1'] == null || $_POST['q2'] == null || $_POST['q3'] == null || $_POST['q4'] == null || $_POST['q5'] == null || $_POST['q6'] == null || $_POST['q7'] == null || $_POST['q8'] == null || $_POST['q9'] == null || $_POST['q10'] == null || $_POST['q11'] == null || $_POST['q12'] == null || $_POST['q13'] == null || $_POST['q14'] == null || $_POST['q15'] == null || $_POST['q16'] == null) {
		if ($_POST['choices-hidden-val'] == 1) {
			$array['javascript'] = "alert('Please select a valid answers.');";
		} else {

			$content = "";

			#get total score of evaluation
			$score = $_POST['q1'] + $_POST['q2'] + $_POST['q3'] + $_POST['q4'] + $_POST['q5'] + $_POST['q6'] + $_POST['q7'] + $_POST['q8'] + $_POST['q9'] + $_POST['q10'] + $_POST['q11'] + $_POST['q12'] + $_POST['q13'] + $_POST['q14'] + $_POST['q15'] + $_POST['q16'];
			$postEvaluate = $this->model->postEvaluate($score, $_POST['strong_points'], $_POST['improve_teaching'], $_POST['other'], $_POST['teacher_id'], $_POST['subject_id'], $_POST['department'], $_POST['student_id']);
				if ($postEvaluate) {
					//$array['html'] = "<div class='alert alert-success'>Evaluation is successfully posted.</div>";
					//$array['javascript'] = "setTimeout(function(){ window.location.href = '".BASEPATH."evaluations'; },1500);";
		
					$teachers = $this->model->getTeacher($_POST['teacher_id']);
					foreach ($teachers as $row) :
						$teacher = $row['lastname'].", ".$row['firstname']." ".$row['middlename'];
					endforeach;

					$subjects = $this->model->getSubject($_POST['subject_id']);
					foreach ($subjects as $row) :
						$subject = $row['code'];
					endforeach;

					$content = "Thank you for taking the Evaluation. ".$subject." Adviser : ".$teacher." completed.";

					$array['javascript'] = "alert('$content'); setTimeout(function(){ window.location.href = '".BASEPATH."teachers'; },1500);";

				} else {

					$array['javascript'] = "alert('Please select a valid answers.');";
				}

				//$array['html'] = $postEvaluate;

		}

		echo json_encode($array);

	}

	function getTeachersv2() {
		$fields = array('teacher', 'subject');
		$sql = "";
		$where = "";
		$enable_where = 0; //No errors yet
		foreach($fields AS $fieldname) { //Loop trough each field
		  if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname]) || $_POST[$fieldname] == "-") {
		    $sql .= ""; //Display error with field
		    $enable_where += 0; //Yup there are errors
		  } else {
		  	$enable_where += 1;
		  	if ($_POST[$fieldname]) {
		  		$clause = "AND ";
		  	} else {
		  		$clause = " ";
		  	}
		  	$sql .= "`".$fieldname."` = '".$_POST[$fieldname]."' ".$clause;
		  }
		}

		if ($enable_where > 0) {
			$sql = chop($sql," AND ");
			$sql = "SELECT * FROM users WHERE $sql group by teacher";			
		} else {
			$sql = chop($sql," AND ");
			$sql = "SELECT * FROM users $sql group by teacher";
			//$sql = str_replace("WHERE", "", $sql);			
		}

		$data['teachers'] = $this->model->search_teachers($sql);
		//$data['num_eval'] = $this->model->count_eval_teacher($_POST['teacher'], $_POST['department']);

		$this->viewLoader->render('evaluations/admin_search_teachers', $data);
	}


	function foo() {
		return "test";
	}


}
