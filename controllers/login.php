<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Login extends Controller {

	function __construct() {

		
			parent::__construct('main_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {

		$this->viewLoader->render('common/main');

	}

	function login() {

		$username = $_POST['username'];
		$password = $_POST['password'];

		$checkLogin = $this->model->validateLogin($username, md5($password));

		if ($checkLogin == "admin") {
			$array['html'] = "<div class='alert alert-success'>Please wait while we redirect you to website...</div>";
			$array['javascript'] = "setTimeout(function(){ window.location.href='users'; },2500);";
		} else if ($checkLogin == "student") {
			$array['html'] = "<div class='alert alert-success'>Please wait while we redirect you to website...</div>";
			$array['javascript'] = "setTimeout(function(){ window.location.href='profiles'; },2500);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Account not found. Please check your username or password.</div>";
		}

		echo json_encode($array);

	}

}
