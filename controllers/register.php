<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Register extends Controller {

	function __construct() {

		
			parent::__construct('users_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {

		//$data['users'] = $this->model->getAll();
		$this->viewLoader->render('common/register');

	}

	function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }
	    return $str;
	}

	function add() {

		//$password = $this->random_str(12);

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$street_brgy = $_POST['street_brgy'];
		$city = $_POST['city'];
		$province = $_POST['province'];
		$department = $_POST['department'];
		$username = $_POST['stud_number'];
		$password = $_POST['password'];

		if ($department == "-") {
			$array['javascript'] = "alert('Please select a valid department for user');";
		} else {
			$addUser = $this->model->add($firstname, $middlename, $lastname, $street_brgy, $city, $province, $department, $username, $password);

			if ($addUser) {
				$array['html'] = "<div class='alert alert-success'>Successfully registered. You may now login.</div>";
				
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while adding user. Please try again.</div>";
			}
		}

		echo json_encode($array);

	}

	function delete($id) {
		
		$deleteUser = $this->model->delete($id);

		if ($deleteUser) {
			$array['html'] = "<div class='alert alert-success'>Successfully deleted user.</div>";
			$array['javascript'] = "setTimeout(function(){ window.location.href='users'; },2000);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Something went wrong while deleting user. Please try again.</div>";
		}

		echo json_encode($array);

	}


}
