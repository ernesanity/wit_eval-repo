<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Teachers extends Controller {

	function __construct() {

		
			parent::__construct('teachers_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {

		if ($_SESSION['user']['type'] == "admin") {
			$data['teachers'] = $this->model->getAll();
			$data['departments'] = $this->model->getDepartment();
			$data['subjects'] = $this->model->getSubjects();
			$this->viewLoader->render('teachers/teachers', $data);
		} else {
			$data['teachers'] = $this->model->getStudentTeacher($_SESSION['user']['department']);
			$data['subjects'] = $this->model->getStudentSubject($_SESSION['user']['id']);
			$this->viewLoader->render('evaluations/evaluations', $data);
		}
	}

	function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }
	    return $str;
	}

	function add() {

		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$department = $_POST['department'];

		if ($department == "-") {
			$array['javascript'] = "alert('Please select a valid department for teacher');";
		} else {
			$addTeacher = $this->model->add($firstname, $middlename, $lastname, $department);

			if ($addTeacher) {
				$array['html'] = "<div class='alert alert-success'>Successfully added teacher.</div>";
				$array['javascript'] = "$('#add-teacher-modal').modal('hide'); setTimeout(function(){ window.location.href = window.location.href; },1500);";
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while adding teacher. Please try again.</div>";
			}
		}

		echo json_encode($array);

	}

	function edit() {

		$id = $_POST['id'];
		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$department = $_POST['department'];

		if ($department == "-") {
			$array['javascript'] = "alert('Please select a valid department for teacher');";
		} else {
			$editTeacher = $this->model->edit($firstname, $middlename, $lastname, $department, $id);

			if ($editTeacher) {
				$array['html'] = "<div class='alert alert-success'>Successfully edited teacher.</div>";
				$array['javascript'] = "$('#editTeacher').modal('hide'); setTimeout(function(){ window.location.href = window.location.href; },1500);";
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while editing teacher. Please try again.</div>";
			}
		}

		echo json_encode($array);

	}

	function delete($id) {
		
		$deleteUser = $this->model->delete($id);

		if ($deleteUser) {
			$array['html'] = "<div class='alert alert-success'>Successfully deleted teacher.</div>";
			$array['javascript'] = "setTimeout(function(){ window.location.href='teachers'; },2000);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Something went wrong while deleting teacher. Please try again.</div>";
		}

		echo json_encode($array);

	}

	function getTeacher($id) {

		$data['teachers'] = $this->model->getTeacher($id);

		$this->viewLoader->render('teachers/edit_teacher', $data);

	}

	function getTeachers() {
		/*
		$data['teachers'] = $this->model->getTeachers($_POST['department'], $_POST['teacher'], $_POST['subject'], $_POST['semester']);

		$this->viewLoader->render('evaluations/search_teachers', $data);
		*/

		$fields = array('department', 'teacher', 'subject', 'semester');
		$sql = "";
		$where = "";
		$enable_where = 0; //No errors yet
		foreach($fields AS $fieldname) { //Loop trough each field
		  if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname]) || $_POST[$fieldname] == "-") {
		    $sql .= ""; //Display error with field
		    $enable_where += 0; //Yup there are errors
		  } else {
		  	$enable_where += 1;
		  	if ($_POST[$fieldname]) {
		  		$clause = "AND ";
		  	} else {
		  		$clause = " ";
		  	}
		  	$sql .= "u.".$fieldname." = '".$_POST[$fieldname]."' ".$clause;
		  }
		}

		if ($enable_where > 0) {
			$sql = chop($sql," AND ");
			//$sql = "SELECT * FROM users WHERE $sql group by teacher LIMIT 1";
			$sql = "SELECT t.*, u.* FROM teachers as t LEFT JOIN users as u ON t.id = u.teacher WHERE $sql group by u.teacher";
		} else {
			$sql = chop($sql," AND ");
			$sql = "SELECT * FROM teachers WHERE department='".$_POST['department']."'";
			//$sql = str_replace("WHERE", "", $sql);			
		}

		$teacher = $this->model->preSQLQuery($sql);
		$content = "";

		//print_r($teacher);

		if(is_array($teacher)):

			foreach ($teacher as $row) :
				//$t = $this->model->getTeacher($row['teacher']);

				//foreach ($t as $r) :

					$status = countStudentsEvaluated($row['id'], $row['department'], $row['subject']);

					$content .= "<tr>";
					$content .= "<td>".$row['lastname'].", ".$row['firstname']." ".$row['middlename']."</td>";
					$content .= "<td>".getSubjectCode($row['subject'])."</td>";
					$content .= "<td>".$row['title']."</td>";
					$content .= "<td>".getDepartmentName($row['department'])."</td>";
					$content .= "<td>";
						if (checkStudentEval($_SESSION['user']['id'], $row['teacher'])) :
							$content .= '<button class="btn btn-danger btn-sm" disabled>You already evaluated this teacher.</button>';
						else :
							$content .= '<a href="'.BASEPATH.'evaluations/evaluate/'.$row['teacher'].'&subject_id='.$row['subject'].'" class="btn btn-info btn-sm">Evaluate</a>';
						endif;
					$content .= "</td>";
					$content .= "</tr>";
				//endforeach;

			endforeach;

			echo $content;
		else :

			echo "<tr><td colspan='5'>No teachers found.</td></tr>";

		endif;


		//$data['teachers'] = ;

		//$this->viewLoader->render('evaluations/search_teachers', $data);		

	}

	function search_teachers() {

		/*
		$data['teachers'] = $this->model->search_teachers($_POST['school_year'], $_POST['semester'], $_POST['department'], $_POST['teacher'], $_POST['subject']);

		$this->viewLoader->render('teachers/search_teachers', $data);
		*/

		if ($_POST['teacher'] == "-") {
			echo "<script>alert('Please select a valid Teacher');</script>";
		} else if ($_POST['department'] == "-") {
			echo "<script>alert('Please select a valid Department');</script>";
		} else {

			$fields = array('school_year', 'semester', 'department', 'teacher');
			$sql = "";
			$where = "";
			$enable_where = 0; //No errors yet
			foreach($fields AS $fieldname) { //Loop trough each field
			  if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname]) || $_POST[$fieldname] == "-") {
			    $sql .= ""; //Display error with field
			    $enable_where += 0; //Yup there are errors
			  } else {
			  	$enable_where += 1;
			  	if ($_POST[$fieldname]) {
			  		$clause = "AND ";
			  	} else {
			  		$clause = " ";
			  	}
			  	$sql .= "`".$fieldname."` = '".$_POST[$fieldname]."' ".$clause;
			  }
			}

			if ($enable_where > 0) {
				$sql = chop($sql," AND ");
				$sql = "SELECT * FROM users WHERE $sql group by teacher LIMIT 1";			
			} else {
				$sql = chop($sql," AND ");
				$sql = "SELECT * FROM users $sql group by teacher LIMIT 1";
				//$sql = str_replace("WHERE", "", $sql);			
			}


			//$data['fields'] = $sql;
			$data['teachers'] = $this->model->search_teachers($sql);
			$data['num_eval'] = $this->model->count_eval_teacher($_POST['teacher'], $_POST['department']);

			$this->viewLoader->render('teachers/search_teachers', $data);

		}

	}

	function getTeachersv3() {
		//echo $_GET['school_year']." ".$_GET['semester']." ".$_GET['department'];

		/*if ($_GET['school_year'] != null) $school_year = $_GET['school_year'];
		else $school_year = "";

		if ($_GET['semester'] != "-") $semester = $_GET['semester'];
		else $semester = "";

		if ($_GET['department'] != "-") $department = $_GET['department'];
		else $department = "";*/

		//$getTeachersv3 = $this->model->getTeachersv3($school_year, $semester, $department);

		$fields = array('school_year', 'semester', 'department');
		$sql = "";
		$where = "";
		$enable_where = 0; //No errors yet
		foreach($fields AS $fieldname) { //Loop trough each field
		  if(!isset($_GET[$fieldname]) || empty($_GET[$fieldname]) || $_GET[$fieldname] == "-") {
		    $sql .= ""; //Display error with field
		    $enable_where += 0; //Yup there are errors
		  } else {
		  	$enable_where += 1;
		  	if ($_GET[$fieldname]) {
		  		$clause = "AND ";
		  	} else {
		  		$clause = " ";
		  	}
		  	$sql .= "`".$fieldname."` = '".$_GET[$fieldname]."' ".$clause;
		  }
		}

		if ($enable_where > 0) {
			$sql = chop($sql," AND ");
			$sql = "SELECT DISTINCT * FROM users WHERE $sql GROUP BY teacher";			
		} else {
			$sql = chop($sql," AND ");
			$sql = "SELECT DISTINCT * FROM users $sql GROUP BY teacher";
			//$sql = str_replace("WHERE", "", $sql);			
		}

		//echo $sql;

		$getTeachersv3 = $this->model->getTeachersv3($sql);
		
		$content = "";
		$content .= "<option value='-'>-</option>";
		
		if ($getTeachersv3) :

			foreach (@$getTeachersv3 as $row) :
				$teachers = $this->model->getTeacher($row['teacher']);
				//echo $row['teacher'];
				if (@$teachers) :
					foreach (@$teachers as $teacher) :
						$content .= "<option value='".$row['teacher']."'>".@$teacher['lastname'].", ".@$teacher['firstname']." ".@$teacher['middlename']."</option>";
					endforeach;
				endif;
					//echo @$teacher['lastname']."<br />";
			endforeach;

		else :

			$content .= "<option value='-'>No teachers found.</option>";

		endif;


		echo $content;

	}

	function getTeachersComments() {

		$getTeachersComments = $this->model->getTeachersComments($_GET['t_id'], $_GET['t_department'], $_GET['t_subject']);

		$content = "";
		if ($getTeachersComments) :

			foreach ($getTeachersComments as $row) :
				$content .= "<div style='border-radius: 0.3em;border: 1px solid #ccc;padding: 10px 10px;margin-bottom:20px;'>";
					$content .= '<div class="form-group">';
					$content .= '<label>1. What are the strong points of your teacher?</label>';
					$content .= '<textarea class="form-control" name="strong_points" readonly>'.$row['strong_points'].'</textarea>';
					$content .= '</div>';

					$content .= '<div class="form-group">';
					$content .= '<label>2. How may your teacher improve his/her teaching?</label>';
					$content .= '<textarea class="form-control" name="strong_points" readonly>'.$row['improve_teaching'].'</textarea>';
					$content .= '</div>';

					$content .= '<div class="form-group">';
					$content .= '<label>3.Other comments.</label>';
					$content .= '<textarea class="form-control" name="strong_points" readonly>'.$row['other'].'</textarea>';
					$content .= '</div>';
				$content .= "</div>";
			endforeach;

		else:

			$content .= "<div class='alert alert-danger'>No comments</div>";

		endif;

		echo $content;
	}


}
