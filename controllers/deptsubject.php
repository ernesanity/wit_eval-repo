<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Deptsubject extends Controller {

	function __construct() {

		
			parent::__construct('deptsubject_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {
		$data['departments'] = $this->model->getDept();
		$data['subjects'] = $this->model->getSubjects();
		$this->viewLoader->render('dept_subject/dept_subject', $data);

	}

	function addSubject() {

		if ($_POST['department'] == "-") {
			$array['javascript'] = "Please select a valid department.";
		} else {

			$addSubject = $this->model->addSubject($_POST['code'], $_POST['department']);

			if ($addSubject) {
				$array['html'] = "<div class='alert alert-success'>Subject has been successfully added.</div>";
				$array['javascript'] = "$('#add-subject-modal').modal('hide'); setTimeout(function(){  window.location.href = window.location.href; resetForm('form-add-subject'); },1500);";
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while trying to add subject.</div>";
				$array['javascript'] = "setTimeout(function(){ resetForm('form-add-subject'); },1500);";
			}

		}

		echo json_encode($array);

	}

	function addDepartment() {

		$addDepartment = $this->model->addDepartment($_POST['dept_name']);

		if ($addDepartment) {
			$array['html'] = "<div class='alert alert-success'>Department has been successfully added.</div>";
			$array['javascript'] = "$('#add-department-modal').modal('hide'); setTimeout(function(){  window.location.href = window.location.href; resetForm('form-add-department'); },1500);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Something went wrong while trying to add department.</div>";
			$array['javascript'] = "setTimeout(function(){ resetForm('form-add-department'); },1500);";
		}

		echo json_encode($array);

	}


}
