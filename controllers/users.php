<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Users extends Controller {

	function __construct() {

		
			parent::__construct('users_model');
			 	
			$this->session=new Session();
			$this->session->start();
		/*	
		if (!$this->session->get('loggedIn') || !($this->session->get('username'))) {
			header('location:' . BASEPATH . 'login');
		}
		*/ 
	}

	function index() {

		$data['users'] = $this->model->getAll();
		$data['departments'] = $this->model->getDept();
		//$data['teachers'] = $this->model->getTeachers();
		//$data['subjects'] = $this->model->getSubjects();
		$this->viewLoader->render('users/users', $data);

	}

	function generateRandomString($length = 12) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function add() {

		//$password = $this->random_str(12);

		$student_num = $_POST['student_num'];
		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dob = $_POST['dob'];
		$street_brgy = $_POST['street_brgy'];
		$city = $_POST['city'];
		$province = $_POST['province'];
		$course = $_POST['course'];
		$department = $_POST['department'];
		$age = $_POST['age'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$year = $_POST['year'];
		//$username = $_POST['username'];


		if ($department == "-") {
			$array['javascript'] = "alert('Please select a valid department for student.');";
		} else if ($password == null || $password == "") {
			$array['javascript'] = "alert('Please generate a valid password for student.');";
		} else {
			$addUser = $this->model->add($student_num, $firstname, $middlename, $lastname, $dob, $street_brgy, $city, $province, $course, $age, $year, $email, $department, $password);

			if ($addUser) {
				$array['html'] = "<div class='alert alert-success'>Successfully added student.</div>";
				$array['javascript'] = "$('#myModal').modal('hide'); setTimeout(function(){ $('#user-list').load(' #user-table'); resetForm('#myModal'); },1500);";
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while adding student. Please try again.</div>";
			}
		}

		echo json_encode($array);

	}

	function edit() {

		//$password = $this->random_str(12);

		$student_num = $_POST['student_num'];
		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$dob = $_POST['dob'];
		$street_brgy = $_POST['street_brgy'];
		$city = $_POST['city'];
		$province = $_POST['province'];
		$course = $_POST['course'];
		$department = $_POST['department'];
		$age = $_POST['age'];
		$email = $_POST['email'];
		$year = $_POST['year'];
		$id = $_POST['id'];
		//$username = $_POST['username'];


		if ($department == "-") {
			$array['javascript'] = "alert('Please select a valid department for student.');";
		} else {
			$editUser = $this->model->edit($student_num, $firstname, $middlename, $lastname, $dob, $street_brgy, $city, $province, $course, $age, $year, $email, $department, $id);

			if ($editUser) {
				$array['html'] = "<div class='alert alert-success'>Successfully edited student.</div>";
				$array['javascript'] = "$('#editStudent').modal('hide'); setTimeout(function(){ $('#user-list').load(' #user-table') },1500);";
			} else {
				$array['html'] = "<div class='alert alert-danger'>Something went wrong while editing student. Please try again.</div>";
			}
		}

		echo json_encode($array);

	}

	function delete($id) {
		
		$deleteUser = $this->model->delete($id);

		if ($deleteUser) {
			$array['html'] = "<div class='alert alert-success'>Successfully deleted user.</div>";
			$array['javascript'] = "setTimeout(function(){ window.location.href='users'; },2000);";
		} else {
			$array['html'] = "<div class='alert alert-danger'>Something went wrong while deleting user. Please try again.</div>";
		}

		echo json_encode($array);

	}

	function generatePass() {

		$password = $this->generateRandomString(12);

		if ($password != null) {
			$array['javascript'] = "$('#password').val('$password');";
		}

		echo json_encode($array);

	}

	
	function search() {

		$department = $_POST['department'];
		$keyword = $_POST['keyword'];

		$search = $this->model->search($department, $keyword);
		
		if (!$search) {
			echo "<tr><td colspan='4'>No records found.</td></tr>";
		} else {
			$data['users'] = $this->model->search($department, $keyword);
			$this->viewLoader->render('users/search_users', $data);
		}
		/*
		$str = "SELECT * FROM students WHERE firstname LIKE '%$keyword%' OR middlename LIKE '%$keyword%' OR lastname LIKE '%$keyword%'";
		$array['html'] = $str;
		*/
		//echo json_encode($array);	

	}
	

	function addUser() {

		$count = count(@$_POST['stud_id']);
		$success_insert = 0;
		$fail_insert = 0;
		$existing_user = 0;
		//$content = "";

		if (@$_POST['stud_default'] == 1) {
			$array['javascript'] = "alert('Student list is empty! Please select students to proceed.');";
		} else if (@$_POST['teacher'] == "-") {
			$array['javascript'] = "alert('Please select a valid teacher to proceed.');";
		} else if (@$_POST['subject'] == "-") {
			$array['javascript'] = "alert('Please select a valid subject to proceed.');";
		} else {

			//$array['javascript'] = "alert('$count');";

			for ($i=0;$i<$count;$i++) {
				$id = $_POST['stud_id'][$i];

				$checkUser = $this->model->checkUser($_POST['teacher'], $_POST['subject'], $id);

				if (!$checkUser) {
					$addUser = $this->model->addUser($_POST['school_year'], $_POST['semester'], $_POST['department'], $_POST['teacher'], $_POST['subject'], $_POST['title'], $id);

					if ($addUser) {
						$success_insert += 1;
						$fail_insert += 0;
					} else {
						$success_insert += 0;
						$fail_insert += 1;
					}
				} else {
					$existing_user += 1;
				}

			}

			$teachers = $this->model->getTeacher($_POST['teacher']);
			foreach ($teachers as $row) :
				$teacher = $row['lastname'].", ".$row['firstname']." ".$row['middlename'];
			endforeach;

			$subjects = $this->model->getSubject($_POST['subject']);
			foreach ($subjects as $row) :
				$subject = $row['code'];
			endforeach;

			//$content .= "Successfully inserted students : ".$success_insert."<br />";
			//$content .= "Failed inserted students : ".$fail_insert."<br />";
			if ($success_insert > 0) {
				$content = "Class list for ".$teacher." for ".$subject." has been created successfully. Total Students : ".$success_insert;
			} else if ($fail_insert > 0) {
				$content = "Class list for ".$teacher." for ".$subject." has been unsuccessfully created.";
			} else if ($existing_user > 0) {
				$content = "Class list for ".$teacher." for ".$subject." has been unsuccessfully created. Reason : Students already existing in this class list.";
			}

			$array['javascript'] = "alert('$content');";			

		}		

		echo json_encode($array);

	}

	function getStudent($id) {

		$data['students'] = $this->model->getStudent($id);

		$this->viewLoader->render('users/edit_student', $data);

	}

	function getDepartmentSubject($department) {

		$data['subjects'] = $this->model->getSubjectByDepartment($department);

		$this->viewLoader->render('users/subjects', $data);

		//echo json_encode($array);

	}

	function getDepartmentTeacher($department) {

		$data['teachers'] = $this->model->getteacherByDepartment($department);

		$this->viewLoader->render('users/teachers', $data);

		//echo json_encode($array);

	}

	function filter_class_list() {
			$fields = array('school_year', 'semester', 'department', 'teacher','subject','title');
			$sql = "";
			$where = "";
			$enable_where = 0; //No errors yet
			foreach($fields AS $fieldname) { //Loop trough each field
			  if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname]) || $_POST[$fieldname] == "-") {
			    $sql .= ""; //Display error with field
			    $enable_where += 0; //Yup there are errors
			  } else {
			  	$enable_where += 1;
			  	if ($_POST[$fieldname]) {
			  		$clause = "AND ";
			  	} else {
			  		$clause = " ";
			  	}
			  	$sql .= "`".$fieldname."` = '".$_POST[$fieldname]."' ".$clause;
			  }
			}

			if ($enable_where > 0) {
				$sql = chop($sql," AND ");
				$sql = "SELECT * FROM users WHERE $sql group by student_id";			
			} else {
				$sql = chop($sql," AND ");
				$sql = "SELECT * FROM users $sql group by student_id";
				//$sql = str_replace("WHERE", "", $sql);			
			}

			//echo $sql;

			$result = $this->model->preSQLQuery($sql);

			$student_list_hidden = "";
			$student_list = "";

			foreach ($result as $row):
				$student = $this->model->getStudent($row['student_id']);

				foreach ($student as $rs):
					$student_list_hidden .= '<input type="hidden" id="stud_id-'.$rs['id'].'" name="stud_id[]" value="'.$rs['id'].'" />';
					$student_list .= '<button id="stud_btn-'.$rs['id'].'" class="btn btn-danger btn-xs" style="font-size: 9px;margin-right: 10px;" onclick="removeStudentToList('.$rs['id'].'); return false;">x</button><input type="text" class="form-control" style="width:80%;display:inline-block;margin-bottom:10px;" id="stud_name-'.$rs['id'].'" name="stud_name[]" value="'.$rs['lastname'].', '.$rs['firstname'].' '.$rs['middlename'].'" readonly/><br />';
				endforeach;

			endforeach;

			$array['student_list_hidden'] = $student_list_hidden;
			$array['student_list'] = $student_list;

			echo json_encode($array);
	}


}
