<?php
/**
 * Main page controller example
 * 
 * TODO form and request helper consider to use symfony2 request component
 */
class Logout extends Controller {

	function __construct() {

		
			parent::__construct('logout_model');
			 	
			$this->session=new Session();
			$this->session->start();
		 
	}

	function index() {

		//$this->model->logout($this->session);
		session_destroy();
		header('refresh:3;url=' . BASEPATH);

	}	

}//class Main extends Controller