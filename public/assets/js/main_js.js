$(document).ready(function(){


  //make ajax request as object
  AjaxObject = {
    startRequest: function(t, u, form, l) {
      postData = $("#"+form).serialize();
      console.log(postData);
      $.ajax({
        type: t,
        url: u,
        data: postData,
        dataType: "JSON",
        beforeSend: function() {
          eval(l);
        },
        success: function(d) {
          console.log(d['javascript']);
          //console.log(d['javascript']);
          $('.loading').html(d['html']);
          eval(d['javascript']);
        }
      });

      return false;
    }
  } //AjaxObject = {

  successMessageModal = function(user, string) {
    $("#add-user-notif").modal('show');

    message = "<div class='alert alert-success'>"+user+"\'s password is : <b>"+string+"</b></div>";

    $("#add-user-notif .modal-body").html(message);    
  }

  $("#add-user-notif").on('hidden.bs.modal', function(){
    window.location.href = window.location.href;
  });

  //$('#user-table').DataTable();
  //$('#teacher-table').DataTable();

  reloadTable = function(tbl, delay) {
     
    setTimeout( function () {
        $('#user-table').DataTable().ajax.reload();
    }, delay );

  }

  deleteUser = function(id) {

    if (confirm('Are you sure you want to delete this user?')) {
      AjaxObject.startRequest('post','users/delete/'+id,'form-delete');
    }

  }

  deleteTeacher = function(id) {

    if (confirm('Are you sure you want to delete this teacher?')) {
      AjaxObject.startRequest('post','teachers/delete/'+id,'form-delete');
    }

  }

  addToList = function(id, stud_name) {

    hidden_value = "";
    student_name = "";

    hidden_value += '<input type="hidden" id="stud_id-'+id+'" name="stud_id[]" value="'+id+'" />';
    student_name += '<button id="stud_btn-'+id+'" class="btn btn-danger btn-xs" style="font-size: 9px;margin-right: 10px;" onclick="removeStudentToList('+id+'); return false;">x</button><input type="text" class="form-control" style="width:80%;display:inline-block;margin-bottom:10px;" id="stud_name-'+id+'" name="stud_name[]" value="'+stud_name+'" readonly/><br />';

    if ($("#students-list-hidden #stud_id-"+id).length > 0) {
      alert('Student is already in list.');
    } else {

      if ($("#students-list-hidden #stud_default").length > 0) {
        $("#students-list-hidden #stud_default").remove();
      } 

      $("#students-list-hidden").append(hidden_value);
      $("#students-list").append(student_name);
    }

  }

  removeStudentToList = function(id) {

    count = $("#students-list-hidden input[type=hidden]").length;

    if (count == 1) {
      $("#students-list-hidden").append('<input type="hidden" id="stud_default" name="stud_default" value="1" />');
    }

    $("#students-list-hidden #stud_id-"+id).remove();
    $("#students-list #stud_btn-"+id).remove();
    $("#students-list #stud_name-"+id).remove();

  }

  editStudent = function(id) {

    $.ajax({
      url: 'users/getStudent/'+id,
      success: function(d) {

        $("#editStudent").modal("show");

        loading = '<div class="alert alert-warning">Fetching data... Please wait...</div>';
        $("#editStudent .modal-body").html(loading);
        setTimeout(function(){
          $("#editStudent .modal-body").html(d);
        },1500);
      }
    });

  }

  resetForm = function(form) {
    document.getElementById(form).reset();
  }

  editTeacher = function(id) {

    $.ajax({
      url: 'teachers/getTeacher/'+id,
      success: function(d) {

        $("#editTeacher").modal("show");

        loading = '<div class="alert alert-warning">Fetching data... Please wait...</div>';
        $("#editTeacher .modal-body").html(loading);
        setTimeout(function(){
          $("#editTeacher .modal-body").html(d);
        },1500);
      }
    });

  }

  searchData = function(url, form, table) {

    postData = $("#"+form).serialize();
    console.log(postData);
    $.ajax({
      url: url,
      type: "POST",
      data: postData,
      success: function(d) {

        //$("#editTeacher").modal("show");

        loading = '<tr><td colspan="4">Searching... Please wait...</td></tr>';
        $("#"+table+" tbody").html(loading);
        setTimeout(function(){
          $("#"+table+" tbody").html(d);
        },1500);
      }
    });

  }

  filterClassList = function(url, form) {

    postData = $("#"+form).serialize();
    //console.log(postData);
    $.ajax({
      url: url,
      type: "POST",
      data: postData,
      dataType: "JSON",
      success: function(d) {
        console.log(d);

        $("#students-list-hidden").html(d['student_list_hidden']);
        $("#students-list-wrapper #students-list").html(d['student_list']);
      }
    });

  }

  searchTeacher = function(url, form, div) {

    postData = $("#"+form).serialize();
    console.log(postData);
    $.ajax({
      url: url,
      type: "POST",
      data: postData,
      success: function(d) {

        //$("#editTeacher").modal("show");

        loading = '<div class="alert alert-warning">Searching... Please wait...</div>';
        $("#"+div).html(loading);
        setTimeout(function(){
          $("#"+div).html(d);
        },1500);
      }
    });

  }

  getSubjectByDepartment = function(id) {
    console.log(id);

    if (id == "-") {
      alert('Please select a valid department.');
    } else {
        $.ajax({
        url: 'users/getDepartmentSubject/'+id,
        success: function(d) {

          //$("#teachers select").removeAttr("disabled").html(d['teachers']);
          $("#subjects").removeAttr("disabled").html(d);

        }
      });
    }
  }

  getTeacherByDepartment = function(id) {
    console.log(id);

    if (id == "-") {
      alert('Please select a valid department.');
    } else {
        $.ajax({
        url: 'users/getDepartmentTeacher/'+id,
        success: function(d) {

          //$("#teachers select").removeAttr("disabled").html(d['teachers']);
          $("#teachers").removeAttr("disabled").html(d);

        }
      });
    }
  }

  getTeachers = function(school_year, semester, department) {

    console.log('teachers/getTeachersv3&school_year='+school_year+'&semester='+semester+'&department='+department);

    $.ajax({
      url: 'teachers/getTeachersv3&school_year='+school_year+'&semester='+semester+'&department='+department,
      type: 'GET',
      success: function(d) {
        //console.log(d);

        $("#teacher").removeAttr("disabled");
        $("#teacher").html(d);
      }
    });

  }

  getTeacherComments = function(t_id, t_department, t_subject) {
    //console.log('teachers/getTeachersComments&t_id='+t_id+'&t_department='+t_department+'&t_subject='+t_subject);

    $("#teacher-comment").modal("show");

    $.ajax({
      url: 'teachers/getTeachersComments&t_id='+t_id+'&t_department='+t_department+'&t_subject='+t_subject,
      type: 'GET',
      beforeSend: function() {
        $("#teacher-comment .modal-body").html("<div class='alert alert-warning'>Fetching comments...</div>");
      },
      success: function(d) {
        //console.log(d);
        $("#teacher-comment .modal-body").html(d);

      }
    });    

  }

  userNotif = function(content) {

    $("#user-notif").modal("show");

    $("#user-notif .modal-body").html(content);

  }

  clearHiddenVal = function() {
    $("#form-post-evaluate input[name=choices-hidden-val]").val(0); 
  }

});