<?php
define('DEVELOPMENT',true);


define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'wit_rating');
define('DB_USER', 'root');
define('DB_PASS', '');

 
define('BASEPATH', 'http://localhost/wit_eval/');
define('MODELPATH','models/');
define('CONTROLLERPATH','controllers/');
define('VIEWPATH','views/');
define('LIBPATH','libs/');
 
define('DEFAULTCONTROLLER', 'main');
 

DEFINE('SESSION_SALT',  'SALT_FOR_SESSION');
