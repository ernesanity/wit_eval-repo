<?php

$database = new Database();
$db = $database->dbConnect();

function getTeacherRating($id) {

	global $db;

	$getRating = $db->prepare("SELECT * FROM evaluation WHERE teacher_id=:teacher_id");
	$getRating->execute(array(":teacher_id" => $id));

	if ($getRating->rowCount() > 0) {

		#count the total of eval of the teacher

		$getCountScore = $db->prepare("SELECT COUNT(id) as count, SUM(score) as score FROM evaluation WHERE teacher_id=:teacher_id");
		$getCountScore->execute(array(":teacher_id" => $id));

		$rsCountScore = $getCountScore->fetch(PDO::FETCH_ASSOC);

		//return $rsCountScore['score'];

		$count = $rsCountScore['count'];

		$total = $count * 16;

		$rs = $getRating->fetch(PDO::FETCH_ASSOC);

		$rating = $rsCountScore['score'] / $total;

		$rating = $rating * 100;

		$rating = $rating / 5;

		return $rating;

	} else {

		return "No ratings yet.";
	}

}

function checkAssessment($u_id) {

	global $db;

	$stmt = $db->prepare("SELECT firsttime_login as assessment FROM students WHERE id=:id");
	$stmt->execute(array(":id" => $u_id));

	if ($stmt->rowCount() > 0) {
		$rs = $stmt->fetch(PDO::FETCH_ASSOC);
		return $rs['assessment'];
	} else {
		return false;
	}

}

function checkStudentEval($u_id, $t_id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM evaluation WHERE teacher_id=:teacher_id AND student_id=:student_id");
	$stmt->execute(array(":teacher_id" => $t_id, ":student_id" => $u_id));

	if ($stmt->rowCount() > 0) {
		return true;
	} else {
		return false;
	}

}

function getDept() {

	global $db;

	$stmt = $db->prepare("SELECT * FROM department");
	$stmt->execute();

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetchAll();
		return $rs;
	} else {
		return false;
	}

}

function getDepartmentName($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM department WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetch(PDO::FETCH_ASSOC);
		return $rs['dept_name'];
	} else {
		return false;
	}

}

function getSubjectCode($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM subjects WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetch(PDO::FETCH_ASSOC);
		return $rs['code'];
	} else {
		return false;
	}

}

function getTeacherName($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM teachers WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetch(PDO::FETCH_ASSOC);
		return $rs['lastname'].", ".$rs['firstname']." ".$rs['middlename'];
	} else {
		return false;
	}

}

function cleanDate($date) {

	$dateObj = DateTime::createFromFormat('Y-m-d', $date);
	$cleanDate = $dateObj->format('M d, Y');

	return $cleanDate;

}

function getTeacher($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM teachers WHERE id=:id");
	$stmt->execute(array(":id" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetchAll();
		return $rs;
	} else {
		return false;
	}

}

function getTeacherSubjects($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM users WHERE teacher=:teacher GROUP BY teacher");
	$stmt->execute(array(":teacher" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetchAll();
		return $rs;
	} else {
		return false;
	}

}

function getSubject($id) {

	global $db;

	$stmt = $db->prepare("SELECT * FROM subjects WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	if ($stmt->rowCount() > 0) {

		$rs = $stmt->fetchAll();
		return $rs;
	} else {
		return false;
	}

}

function teacherStatus($id, $department, $subject) {

	global $db;

	$stmt = $db->prepare("SELECT COUNT(id) as count FROM evaluation WHERE teacher_id=:teacher_id AND department=:department AND subject_id=:subject_id");
	$stmt->execute(array(":teacher_id" => $id, ":department" => $department, ":subject_id" => $subject));

	$rsCountEval = $stmt->fetch(PDO::FETCH_ASSOC);

	return $rsCountEval['count'];

}

function countStudentsEvaluated($t_id, $t_department, $t_subject) {

	global $db;

	$stmt = $db->prepare("SELECT COUNT(student_id) as count FROM users WHERE teacher=:teacher AND department=:department AND subject=:subject");
	$stmt->execute(array(":teacher" => $t_id, ":department" => $t_department, ":subject" => $t_subject));

	$rsCountEval = $stmt->fetch(PDO::FETCH_ASSOC);

	return $rsCountEval['count'];

}